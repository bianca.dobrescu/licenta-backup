import React from "react";
import { Container, Button } from "reactstrap";
import NavigationBar from "./navigation-bar";
import { withRouter } from "react-router";
import ScrollUpButton from "react-scroll-up-button";
import Footer from "rc-footer";
import "rc-footer/assets/index.css";
import {
  Polyline,
  DirectionsRenderer,
  withGoogleMap,
  GoogleMap,
  Marker,
} from "react-google-maps";

class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      email: "",
      latitudeOrigin: 46.76812,
      longitudeOrigin: 23.58407,
      latitudeDestination: null,
      longitudeDestination: null,
      height: props.height,
      width: props.width,
      directionsBool: false,
      directions: [],
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    console.log("making request");
    fetch("/result")
      .then((response) => {
        console.log(response);
        return response.json();
      })
      .then((json) => {
        console.log = json;
        this.setState({ email: json[0] });
      });
  }

  componentWillMount() {
    this.setState({
      height: window.innerHeight / 1.4 + "px",
      width: window.innerWidth / 1.4 + "px",
    });
  }

  componentDidMount() {
    const google = window.google;
    const directionsService = new google.maps.DirectionsService();

    const origin = {
      lat: 46.76812,
      lng: 23.58407,
    };
    const destination = {
      lat: 46.30897,
      lng: 23.73511,
    };

    directionsService.route(
      {
        origin: origin,
        destination: destination,
        travelMode: google.maps.TravelMode.DRIVING,
      },
      (result, status) => {
        if (status === google.maps.DirectionsStatus.OK) {
          this.setState({
            directions: result.routes[0].overview_path,
          });
        } else {
          console.error(`error fetching directions ${result}`);
        }
      }
    );
  }

  position = async () => {
    await navigator.geolocation.getCurrentPosition(
      (position) =>
        this.setState({
          latitudeDestination: position.coords.latitude,
          longitudeDestination: position.coords.longitude,
          directionsBool: true,
        }),
      (err) => console.log(err)
    );
    console.log(this.state.latitude);
  };

  render() {
    const GoogleMapExample = withGoogleMap((props) => (
      <div>
        <GoogleMap
          defaultCenter={{
            lat: this.state.latitudeOrigin,
            lng: this.state.longitudeOrigin,
          }}
          defaultZoom={8}
        >
          <Marker
            position={{
              lat: this.state.latitudeOrigin,
              lng: this.state.longitudeOrigin,
            }}
          />

          <Marker
            position={{
              lat: this.state.latitudeDestination,
              lng: this.state.longitudeDestination,
            }}
          />
          <DirectionsRenderer directions={this.state.directions} />

          {this.state.directionsBool && (
            <Polyline
              path={[
                {
                  lat: this.state.latitudeOrigin,
                  lng: this.state.longitudeOrigin,
                },
                {
                  lat: this.state.latitudeDestination,
                  lng: this.state.longitudeDestination,
                },
              ]}
              geodesic={true}
              options={{
                strokeColor: "#db3e00",
                strokeOpacity: 1,
                strokeWeight: 3,
                icons: [
                  {
                    icon: "hello",
                    offset: "0",
                    repeat: "20px",
                  },
                ],
              }}
            />
          )}
        </GoogleMap>
      </div>
    ));

    return (
      <div>
        <NavigationBar />
        <Container fluid>
          <h1 className="display-4">Contact cabinet oftalmologic</h1>
          <p>
            <b>Adresa: 400001, str. Moţilor 9, ap. 8, Cluj-Napoca, România</b>
          </p>
          <p>
            <b>Telefon/fax: 0364.107.206</b>
          </p>
          <p>
            <b>Email: ofta@rreview.ro</b>
          </p>
          <p>
            <b>
              Website: <a href="http://www.lentile-de-noapte.eu/">OftaReview</a>
            </b>
          </p>
        </Container>
        <Container>
          <div className="card-body">
            <button
              class="btn btn-primary btn-lg btn-block"
              onClick={this.position}
            >
              Afla distanta pana la cabinetul oftalmologic
            </button>
          </div>
          <h2> </h2>
          <GoogleMapExample
            containerElement={
              <div style={{ height: `600px`, width: this.state.width }} />
            }
            mapElement={<div style={{ height: `100%` }} />}
          />
        </Container>
        <p> </p>
        <ScrollUpButton
          style={{
            backgroundColor: "#3385df",
          }}
        />
      </div>
    );
  }
}

export default withRouter(Contact);
