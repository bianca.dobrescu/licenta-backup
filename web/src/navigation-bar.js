import React from "react";
import logo from "./images/5.png";
import { Button } from "reactstrap";
import { withRouter } from "react-router";
import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  Navbar,
  NavbarBrand,
  NavLink,
  UncontrolledDropdown,
} from "reactstrap";

const textStyle = {
  color: "#2196F3",
  textDecoration: "none",
};

const signOut = () => {
  fetch("http://localhost:81/php/src/signout.php", {
    method: "POST",
    credentials: "include",
    body: JSON.stringify({}),
  })
    .then((response) => response.json())
    .then((responseJson) => {
      if (responseJson === "delogat") {
        this.props.history.push("/login");
        alert(responseJson);
      }
    })
    .catch((error) => {
      console.error(error);
    });
};

const NavigationBar = () => (
  <div>
    <Navbar color="dark" light expand="md">
      <NavbarBrand href="/home">
        <img src={logo} width={"55"} height={"55"} />
      </NavbarBrand>
      <Nav className="mr-auto" navbar>
        <UncontrolledDropdown nav inNavbar>
          <DropdownToggle style={textStyle} nav caret>
            Meniu
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem>
              <NavLink href="/home">Acasa</NavLink>
            </DropdownItem>

            <DropdownItem>
              <NavLink href="/profil">Profil</NavLink>
            </DropdownItem>

            <DropdownItem>
              <NavLink href="/programari">Programari</NavLink>
            </DropdownItem>

            <DropdownItem>
              <NavLink href="/despre">Despre noi</NavLink>
            </DropdownItem>

            <DropdownItem>
              <NavLink href="/contact">Contact</NavLink>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </Nav>
      <Button color="primary" href="/login" onClick={() => signOut()}>
        Delogheaza-te
      </Button>
    </Navbar>
  </div>
);

export default withRouter(NavigationBar);
