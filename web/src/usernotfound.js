import React from "react";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import { Button } from "reactstrap";

class UserNotFound extends React.Component {
  render() {
    return (
      <div>
        <h3> Nu esti logat! </h3>
        <h4>
          Pentru a accesa site-ul logheaza-te aici{" "}
          <Link to="/login">Login </Link> sau creaza un cont nou{" "}
          <Link to="/register">Register</Link>
        </h4>
        <Button href="/home" color="primary">
          Daca tocmai te-ai logat, apasa aici!
        </Button>
      </div>
    );
  }
}

export default withRouter(UserNotFound);
