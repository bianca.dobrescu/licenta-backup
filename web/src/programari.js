import React from "react";
import NavigationBar from "./navigation-bar";
import { withRouter } from "react-router";
import { Container, Jumbotron } from "reactstrap";
import ScrollUpButton from "react-scroll-up-button";
import "rc-footer/assets/index.css";
import DateTime from "./programari_calendar";
import OrarData from "./OrarData.json";
import Loader from "react-loader-spinner";

var data;
class Programari extends React.Component {
  constructor() {
    super();
    this.state = {
      dataJSON: OrarData.data,
      selectedCalendar: {
        date: "",
        time: "",
      },
      currentDate: new Date(),
      isDateSelected: false,
      isTimeSelected: false,
      confirmBooking: false,
      motivProgramare: "",
      isMotiv: false,
      programari: [],
      programareDateTime: "",
      programareMotiv: "",
      loading: true,
    };
    this.selectedTime = this.selectedTime.bind(this);
    this.onChangeMotiv = this.onChangeMotiv.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChangeMotiv(e) {
    this.setState({
      motivProgramare: e.target.value,
    });
  }

  onCancel(e) {
    e.preventDefault();
    this.setState({
      selectedCalendar: {
        date: "",
        time: "",
      },
      isDateSelected: false,
      isTimeSelected: false,
      confirmBooking: false,
      motivProgramare: "",
    });
  }

  onSubmit(e) {
    e.preventDefault();

    let UserDate = this.state.selectedCalendar.date;
    let UserTime = this.state.selectedCalendar.time;
    let UserMotiv = this.state.motivProgramare;

    fetch("http://localhost:81/phplicenta/src/addRezervare.php", {
      method: "POST",
      credentials: "include",
      body: JSON.stringify({
        date: UserDate,
        time: UserTime,
        motiv: UserMotiv,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        alert(responseJson);
      })
      .catch((error) => {
        console.log("Did not work");
        console.log(error);
        console.error(error);
      });
    this.setState({
      selectedCalendar: {
        date: "",
        time: "",
      },
      isDateSelected: false,
      isTimeSelected: false,
      confirmBooking: false,
      motivProgramare: "",
    });
  }

  DateOnChange = (date) => {
    var date = date.toLocaleDateString();
    this.setState({
      isDateSelected: true,
      selectedCalendar: {
        date: date,
        time: "",
      },
    });
  };

  selectedTime(i) {
    this.setState({
      isTimeSelected: true,
      selectedCalendar: {
        date: this.state.selectedCalendar.date,
        time: i,
      },
    });
  }

  nextStep = (e) => {
    e.preventDefault();
    this.setState({
      confirmBooking: true,
    });
  };

  componentDidMount() {
    fetch("http://localhost:81/phplicenta/src/getRezervari.php", {
      method: "POST",
      credentials: "include",
      body: JSON.stringify({}),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        //data = JSON.stringify(responseJson);
        data = responseJson;
        this.setState({ programari: data, loading: false });
      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    const { programari, loading, rezervari } = this.state;
    return (
      <div>
        <NavigationBar />
        <Jumbotron>
          <Container fluid>
            <div className="container">
              <h1>Selecteaza ziua si ora programarii dorite</h1>
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <DateTime
                  onChange={this.DateOnChange}
                  date={this.state.currentDate}
                  timings={this.state.dataJSON.timings}
                  isDateSelected={this.state.isDateSelected}
                  selectedTime={this.selectedTime}
                  nextStep={this.nextStep}
                  selectedTime2={this.state.selectedCalendar.time}
                />
              </div>

              <h1> </h1>
              <div>
                {this.state.selectedCalendar.date.length > 0 && (
                  <div>
                    <h6>Data programarii:</h6>
                    <div>
                      <li className="list-group-item">
                        <a href="#">{this.state.selectedCalendar.date}</a>
                      </li>
                    </div>
                  </div>
                )}
                {this.state.selectedCalendar.time.length > 0 && (
                  <div>
                    <h6>Ora programarii:</h6>
                    <li className="list-group-item">
                      <a href="#">{this.state.selectedCalendar.time}</a>
                    </li>
                  </div>
                )}
                {this.state.selectedCalendar.time.length > 0 && (
                  <div>
                    <h6>Motivul programarii:</h6>
                    <input
                      type="text"
                      value={this.state.motivProgramare}
                      onChange={this.onChangeMotiv}
                      className="form-control"
                      id="motivProgramare"
                      placeholder="Va rugam completati.."
                    />
                  </div>
                )}
              </div>
              {this.state.confirmBooking ? (
                <div>
                  <div className="card-body">
                    <button
                      class="btn btn-secondary btn-lg btn-block"
                      onClick={this.onCancel}
                    >
                      Anuleaza
                    </button>
                    <button
                      class="btn btn-primary btn-lg btn-block"
                      onClick={this.onSubmit}
                    >
                      Confirma Programarea
                    </button>
                  </div>
                </div>
              ) : null}
            </div>
          </Container>
          <p> </p>
          <p> </p>
          <p> </p>
          <Container>
            {loading ? (
              <div
                style={{
                  width: "100%",
                  height: "100",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Loader
                  type="ThreeDots"
                  color="#2196f3"
                  height="100"
                  width="100"
                />
              </div>
            ) : (
              <form>
                <div style={{ marginTop: 20 }}>
                  <div className="album py-5 bg-light">
                    <div className="container">
                      <h1>Istoric Programari</h1>
                      {programari.map((programare) => (
                        <div>
                          <i style={{ fontSize: 10 }}>
                            Data si ora programarii:
                          </i>
                          <h6>{programare.start}</h6>
                          <ul>
                            <i style={{ fontSize: 10 }}>Motivul programarii:</i>
                            {programare.motiv}
                          </ul>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </form>
            )}
          </Container>
        </Jumbotron>

        <ScrollUpButton
          style={{
            backgroundColor: "#3385df",
          }}
        />
      </div>
    );
  }
}

export default withRouter(Programari);
