import React from "react";
import NavigationBar from "./navigation-bar";
import { withRouter } from "react-router";
import Loader from "react-loader-spinner";
import ScrollUpButton from "react-scroll-up-button";
import Footer from "rc-footer";
import "rc-footer/assets/index.css";

import { Card, CardBody, CardTitle, CardSubtitle, Button } from "reactstrap";
var data;

class Profil extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editing: false,
      loading: true,
      numeprenume: "",
      datanasterii: "",
      cnp: "",
      varsta: "",
      adresa: "",
      telefon: "",
      email: "",
    };

    this.onChangeNumePrenume = this.onChangeNumePrenume.bind(this);
    this.onChangeDataNasterii = this.onChangeDataNasterii.bind(this);
    this.onChangeCNP = this.onChangeCNP.bind(this);
    this.onChangeVarsta = this.onChangeVarsta.bind(this);
    this.onChangeAdresa = this.onChangeAdresa.bind(this);
    this.onChangeTelefon = this.onChangeTelefon.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChangeNumePrenume(e) {
    this.setState({
      numeprenume: e.target.value,
    });
  }

  onChangeDataNasterii(e) {
    this.setState({
      datanasterii: e.target.value,
    });
  }

  onChangeCNP(e) {
    this.setState({
      cnp: e.target.value,
    });
  }

  onChangeVarsta(e) {
    this.setState({
      varsta: e.target.value,
    });
  }

  onChangeAdresa(e) {
    this.setState({
      adresa: e.target.value,
    });
  }

  onChangeTelefon(e) {
    this.setState({
      telefon: e.target.value,
    });
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value,
    });
  }

  onSubmit(e) {
    e.preventDefault();

    console.log(`Form submitted:`);

    let UserNumePrenume = this.state.numeprenume;
    let UserDataNasterii = this.state.datanasterii;
    let UserCNP = this.state.cnp;
    let UserVarsta = this.state.varsta;
    let UserAdresa = this.state.adresa;
    let UserTelefon = this.state.telefon;
    let UserEmail = this.state.email;

    fetch("http://localhost:81/phplicenta/src/modifyProfile.php", {
      method: "POST",
      credentials: "include",
      body: JSON.stringify({
        nume_prenume: UserNumePrenume,
        data_nasterii: UserDataNasterii,
        CNP: UserCNP,
        varsta: UserVarsta,
        adresa: UserAdresa,
        telefon: UserTelefon,
        email: UserEmail,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        alert(responseJson);
      })
      .catch((error) => {
        console.log("Did not work");
        console.log(error);
        console.error(error);
      });
    this.setState({ editing: false });
  }

  componentDidMount() {
    fetch("http://localhost:81/phplicenta/src/getProfile.php", {
      method: "POST",
      credentials: "include",
      body: JSON.stringify({}),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        data = JSON.stringify(responseJson);
        var v = data.split(",");
        this.setState({
          loading: false,
          numeprenume: v[0].substr(1),
          datanasterii: v[1],
          cnp: v[2],
          varsta: v[3],
          adresa: v[4],
          telefon: v[5],
          email: v[6].slice(0, -1),
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    const isEdit = this.state.editing;
    const { loading } = this.state;
    return (
      <div>
        <NavigationBar />
        <div>
          {isEdit ? (
            <div style={{ marginTop: 20 }}>
              <div className="album py-5 bg-light">
                <div className="container">
                  <h1 class="h3 mb-3 font-weight-bold">Modifica profilul</h1>
                  <p>Ofera toate informatiile necesare mai jos</p>
                  <div className="row" />
                  <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                      <label htmlFor="title" className="col-form-label">
                        Nume, prenume:
                      </label>
                      <input
                        type="text"
                        value={this.state.numeprenume}
                        onChange={this.onChangeNumePrenume}
                        className="form-control"
                        id="numeprenume"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="text" className="col-form-label">
                        Data nasterii:
                      </label>
                      <input
                        type="date"
                        value={this.state.datanasterii}
                        onChange={this.onChangeDataNasterii}
                        className="form-control"
                        id="datanasterii"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="title" className="col-form-label">
                        CNP:
                      </label>
                      <input
                        type="text"
                        value={this.state.cnp}
                        onChange={this.onChangeCNP}
                        className="form-control"
                        id="cnp"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="title" className="col-form-label">
                        Varsta:
                      </label>
                      <input
                        type="text"
                        value={this.state.varsta}
                        onChange={this.onChangeVarsta}
                        className="form-control"
                        id="varsta"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="title" className="col-form-label">
                        Adresa:
                      </label>
                      <input
                        type="text"
                        value={this.state.adresa}
                        onChange={this.onChangeAdresa}
                        className="form-control"
                        id="adresa"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="message-text" className="col-form-label">
                        Telefon:
                      </label>
                      <input
                        type="text"
                        value={this.state.telefon}
                        onChange={this.onChangeTelefon}
                        className="form-control"
                        id="telefon"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="message-text" className="col-form-label">
                        Email:
                      </label>
                      <input
                        type="email"
                        value={this.state.email}
                        onChange={this.onChangeEmail}
                        className="form-control"
                        id="email"
                      />
                    </div>

                    <br />
                    <div className="card-body">
                      <button
                        class="btn btn-primary btn-lg btn-block"
                        type="submit"
                      >
                        Schimba
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          ) : (
            <div>
              <p> </p>
              {loading ? (
                <div
                  style={{
                    width: "100%",
                    height: "100",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Loader
                    type="ThreeDots"
                    color="#1273de"
                    height="150"
                    width="150"
                  />
                </div>
              ) : (
                <div style={{ marginTop: 20 }}>
                  <div className="album py-5 bg-light">
                    <div className="container">
                      <h1>Informatii profil</h1>
                      <Card className="lead">
                        <CardBody>
                          <CardTitle>
                            <b>Nume, prenume:</b> {this.state.numeprenume}
                          </CardTitle>
                          <CardSubtitle>
                            <b>Data nasterii:</b> {this.state.datanasterii}
                          </CardSubtitle>
                          <CardSubtitle>
                            <b>Varsta:</b> {this.state.varsta}
                          </CardSubtitle>
                          <CardSubtitle>
                            <b>CNP:</b> {this.state.cnp}
                          </CardSubtitle>
                          <CardSubtitle>
                            <b>Adresa:</b> {this.state.adresa}
                          </CardSubtitle>
                          <CardSubtitle>
                            <b>Telefon:</b> {this.state.telefon}
                          </CardSubtitle>
                          <CardSubtitle>
                            <b>Email:</b> {this.state.email}
                          </CardSubtitle>
                        </CardBody>

                        <div className="card-body">
                          <button
                            class="btn btn-primary btn-lg btn-block"
                            type="submit"
                            onClick={() => this.setState({ editing: true })}
                          >
                            Modifica Profil
                          </button>
                        </div>
                      </Card>
                    </div>
                  </div>
                </div>
              )}
              ;
            </div>
          )}
        </div>

        <ScrollUpButton
          style={{
            backgroundColor: "#3385df",
          }}
        />
      </div>
    );
  }
}

export default withRouter(Profil);
