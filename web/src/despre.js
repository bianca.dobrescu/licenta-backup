import React from "react";
import NavigationBar from "./navigation-bar";
import ScrollUpButton from "react-scroll-up-button";
import { Container } from "reactstrap";
import { withRouter } from "react-router";

class Despre extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
    };
  }
  render() {
    return (
      <div>
        <NavigationBar />
        <p> </p>
        <div className="container">
          <p>
            Deschisă oficial în 2006, prin achiziţionarea de aparatură de înaltă
            performanţă, clinica Ofta Review îşi propune să ofere investigaţii
            oftalmologice avansate şi să se afirme ca un centru de referinţă în
            studiul retinei şi neurooftalmologiei.
          </p>
          <p>
            Este locul ideal unde puteţi găsi răspunsul la întrebările despre
            sănătatea ochilor dumneavoastră. Profesionalismul şi dăruirea
            colectivului medical combinate cu tehnologia de vârf din domeniu,
            utilizată pentru investigarea structurii şi funcţiei oculare, sunt
            garanţia unor consultaţii complexe; ele se finalizează cu un
            diagnostic complet şi indicaţii terapeutice optime.
          </p>
        </div>
        <h1> </h1>
        <Container>
          <div className="App">
            <p>Mai multe informatii legate de cabinetul OftaReview pe mail</p>
            <div>
              <form
                onSubmit={this.handleSubmit}
                action="http://localhost:5000/result"
                method="POST"
              >
                <input
                  type="email"
                  id="email"
                  name="email"
                  placeholder="Email-ul tau.."
                  value={this.state.email}
                  onChange={(e) => this.setState({ email: e.target.value })}
                />

                <div className="card-body">
                  <button
                    class="btn btn-primary btn-lg btn-block"
                    type="submit"
                    onClick={this.handleSubmit}
                  >
                    Primeste Informatii
                  </button>
                </div>
              </form>
            </div>
          </div>
        </Container>
        <p> </p>
        <ScrollUpButton
          style={{
            backgroundColor: "#3385df",
          }}
        />
      </div>
    );
  }
}

export default withRouter(Despre);
