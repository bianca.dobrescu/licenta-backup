import React, { Component } from "react";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import "./index.css";

class DateTime extends Component {
  selectedTime(i) {
    this.props.selectedTime(i);
  }

  render() {
    return (
      <div className="">
        <Calendar
          onChange={this.props.onChange}
          value={this.props.date}
          minDate={this.props.date}
        />

        {this.props.isDateSelected ? (
          <div className="time-list">
            <ul>
              {this.props.timings.map((el, i) => {
                return (
                  <li key={i} onClick={this.selectedTime.bind(this, el)}>
                    <a href="#"> {el}</a>
                  </li>
                );
              })}
            </ul>
          </div>
        ) : null}

        <button
          className="align-center btn-primary btn mt-5"
          disabled={!this.props.selectedTime2}
          onClick={this.props.nextStep}
        >
          Pasul urmator
        </button>
      </div>
    );
  }
}

export default DateTime;
