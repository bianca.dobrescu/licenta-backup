import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./home";
import Contact from "./contact";
import Login from "./login";
import Despre from "./despre";
import Programari from "./programari";
import Profil from "./profil-pacient";
import UserNotFound from "./usernotfound";
import Footer from "rc-footer";
import "rc-footer/assets/index.css";

import ErrorPage from "./errorhandling/error-page";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogged: false,
    };
  }

  componentDidMount() {
    fetch("http://localhost:81/phplicenta/src/isLogged.php", {
      method: "POST",
      credentials: "include",
      body: JSON.stringify({}),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson === "logat") {
          this.setState({ isLogged: true });
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    if (this.state.isLogged) {
      return (
        <div>
          <Router>
            <div>
              <Switch>
                <Route exact path="/" render={() => <Login />} />

                <Route exact path="/home" render={() => <Home />} />

                <Route exact path="/despre" render={() => <Despre />} />

                <Route exact path="/contact" render={() => <Contact />} />

                <Route exact path="/programari" render={() => <Programari />} />

                <Route exact path="/profil" render={() => <Profil />} />

                <Route exact path="/login" render={() => <Login />} />

                <Route exact path="/error" render={() => <ErrorPage />} />

                <Route render={() => <ErrorPage />} />
              </Switch>
              <Footer
                backgroundColor="#3a3a3c"
                columns={[
                  {
                    title: "Link-uri utile",
                    items: [
                      {
                        title: "Profil",
                        url: "http://localhost:3000/profil",
                        openExternal: true,
                      },
                      {
                        title: "Programari",
                        url: "http://localhost:3000/programari",
                        openExternal: true,
                      },
                      {
                        title: "Despre",
                        url: "http://localhost:3000/despre",
                        openExternal: true,
                      },
                      {
                        title: "Contact",
                        url: "http://localhost:3000/contact",
                        openExternal: true,
                      },
                    ],
                  },
                  {
                    title: "Orar cabinet",
                    items: [
                      {
                        title: "Luni-Vineri",
                        description: "09:00-18:00",
                      },
                      {
                        title: "Sambata-Duminica",
                        description: "INCHIS",
                      },
                    ],
                  },

                  {
                    items: [
                      {
                        icon: (
                          <img src="https://ya-webdesign.com/transparent250_/blue-map-pin-png-1.png" />
                        ),
                        title:
                          "400001, str. Moţilor 9, ap. 8, Cluj-Napoca, România",
                      },
                      {
                        icon: (
                          <img src="https://ya-webdesign.com/transparent250_/telephone-clipart-yellow-telephone-10.png" />
                        ),
                        title: "0364.107.206",
                      },
                      {
                        icon: (
                          <img src="https://ya-webdesign.com/transparent250_/mail-message.png" />
                        ),
                        title: "ofta@rreview.ro",
                      },
                    ],
                  },
                ]}
                bottom="©2020 Copyright: Bianca"
              />
            </div>
          </Router>
        </div>
      );
    }
    return (
      <div>
        <Router>
          <div>
            <Switch>
              <Route exact path="/" render={() => <Login />} />

              <Route exact path="/login" render={() => <Login />} />

              <Route exact path="/home" render={() => <UserNotFound />} />

              <Route exact path="/despre" render={() => <UserNotFound />} />

              <Route exact path="/contact" render={() => <UserNotFound />} />

              <Route exact path="/programari" render={() => <UserNotFound />} />

              <Route exact path="/profil" render={() => <UserNotFound />} />

              <Route exact path="/error" render={() => <ErrorPage />} />

              <Route render={() => <ErrorPage />} />
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
