import React, { Component } from "react";
import { withRouter } from "react-router";
import { Container } from "reactstrap";

const initialState = {
  username: "",
  parola: "",
};

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      parola: "",
      isInvalid: "",
    };
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangeParola = this.onChangeParola.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChangeUsername(e) {
    this.setState({
      username: e.target.value,
      isInvalid: "",
    });
  }

  onChangeParola(e) {
    this.setState({
      parola: e.target.value,
      isInvalid: "",
    });
  }

  onSubmit(e) {
    e.preventDefault();

    console.log(`Form submitted:`);

    let UserUsername = this.state.username;
    let UserParola = this.state.parola;

    fetch("http://localhost:81/phplicenta/src/signin.php", {
      method: "POST",
      credentials: "include",
      body: JSON.stringify({
        username: UserUsername,
        parola: UserParola,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson === "logat") {
          this.props.history.push("/home");
        } else {
          //alert(responseJson);
          this.setState({ isInvalid: responseJson + "! Incearca din nou!" });
          this.setState(initialState);
        }
      })
      .catch((error) => {
        console.log("Did not work");
        console.log(error);
        console.error(error);
      });

    this.setState({
      username: "",
      parola: "",
    });
  }
  render() {
    return (
      <Container>
        <div className="App">
          <form className="App" onSubmit={this.onSubmit}>
            <h1 class="h3 mb-3">Logare</h1>

            <label style={{ color: "red" }}>{this.state.isInvalid}</label>

            <div className="form-group">
              <label>Username</label>
              <input
                type="text"
                className="form-control"
                id="username"
                value={this.state.username}
                onChange={this.onChangeUsername}
                placeholder="Username-ul tau.."
              />
            </div>

            <div className="form-group">
              <label>Parola</label>
              <input
                type="password"
                className="form-control"
                placeholder="Parola ta.."
                value={this.state.parola}
                onChange={this.onChangeParola}
                id="parola"
              />
            </div>

            <button type="submit" className="btn btn-primary btn-block">
              Logare
            </button>
          </form>
        </div>
      </Container>
    );
  }
}
export default withRouter(Login);
