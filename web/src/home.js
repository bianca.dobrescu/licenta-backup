import React from "react";
import NavigationBar from "./navigation-bar";
import { Button, Container, Jumbotron } from "reactstrap";
import Flippy, { FrontSide, BackSide } from "react-flippy";
import ScrollUpButton from "react-scroll-up-button";
import Footer from "rc-footer";
import "rc-footer/assets/index.css";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      flippy: "",
      height: props.height,
      width: props.width,
    };
  }

  componentWillMount() {
    this.setState({
      height: window.innerHeight / 2.3 + "px",
      width: window.innerWidth / 5 + "px",
    });
  }

  render() {
    return (
      <div>
        <NavigationBar />
        <Jumbotron>
          <Container fluid>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Flippy
                flipOnHover={true} // default false
                flipOnClick={false} // default false
                flipDirection="horizontal" // horizontal or vertical
                ref={(r) => (this.flippy = r)} // to use toggle method like this.flippy.toggle()
                // if you pass isFlipped prop component will be controlled component.
                // and other props, which will go to div
                style={{ width: "300px", height: "300px" }} /// these are optional style, it is not necessary
              >
                <FrontSide
                  style={{
                    backgroundColor: "#3a3a3c",
                    display: "flex",
                    alignItems: "center",
                    flexDirection: "column",
                  }}
                >
                  <img
                    width="250px"
                    height="250px"
                    src="https://cdn3.iconfinder.com/data/icons/mixed-communication-and-ui-pack-1/48/general_pack_NEW_glyph_profile-512.png"
                  />
                </FrontSide>
                <BackSide
                  style={{
                    backgroundColor: "#3385df",
                    display: "flex",
                    alignItems: "center",
                    flexDirection: "column",
                  }}
                >
                  <h2>Profil</h2>
                  <p className="App">
                    {" "}
                    Verificati si editati informatiile dumneavoastra de profil
                  </p>
                  <a href="http://localhost:3000/profil">
                    <img
                      width="120px"
                      height="120px"
                      src="https://ya-webdesign.com/transparent250_/mouse-click-icon-png-2.png"
                    />
                  </a>
                </BackSide>
              </Flippy>
              <Flippy
                flipOnHover={true} // default false
                flipOnClick={false} // default false
                flipDirection="horizontal" // horizontal or vertical
                ref={(r) => (this.flippy = r)} // to use toggle method like this.flippy.toggle()
                // if you pass isFlipped prop component will be controlled component.
                // and other props, which will go to div
                style={{ width: "300px", height: "300px" }} /// these are optional style, it is not necessary
              >
                <FrontSide
                  style={{
                    backgroundColor: "#3a3a3c",
                    display: "flex",
                    alignItems: "center",
                    flexDirection: "column",
                  }}
                >
                  <img
                    width="250px"
                    height="250px"
                    src="https://cdn1.iconfinder.com/data/icons/office-185/65/event-512.png"
                  />
                </FrontSide>
                <BackSide
                  style={{
                    backgroundColor: "#3385df",
                    display: "flex",
                    alignItems: "center",
                    flexDirection: "column",
                  }}
                >
                  <h2>Programari</h2>
                  <p className="App"> Rezervati o programare aici</p>
                  <a href="http://localhost:3000/programari">
                    <img
                      width="120px"
                      height="120px"
                      src="https://ya-webdesign.com/transparent250_/mouse-click-icon-png-2.png"
                    />
                  </a>
                </BackSide>
              </Flippy>
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Flippy
                flipOnHover={true} // default false
                flipOnClick={false} // default false
                flipDirection="horizontal" // horizontal or vertical
                ref={(r) => (this.flippy = r)} // to use toggle method like this.flippy.toggle()
                // if you pass isFlipped prop component will be controlled component.
                // and other props, which will go to div
                style={{ width: "300px", height: "300px" }} /// these are optional style, it is not necessary
              >
                <FrontSide
                  style={{
                    backgroundColor: "#3a3a3c",
                    display: "flex",
                    alignItems: "center",
                    flexDirection: "column",
                  }}
                >
                  <img
                    width="250px"
                    height="250px"
                    src="https://www.polvac.com/wp-content/uploads/2015/11/about2.png"
                  />
                </FrontSide>
                <BackSide
                  style={{
                    backgroundColor: "#3385df",
                    display: "flex",
                    alignItems: "center",
                    flexDirection: "column",
                  }}
                >
                  <h2>Despre Noi</h2>
                  <p className="App">
                    {" "}
                    Informatii legate de cabinetul oftalmologic OftaReview
                  </p>
                  <a href="http://localhost:3000/despre">
                    <img
                      width="120px"
                      height="120px"
                      src="https://ya-webdesign.com/transparent250_/mouse-click-icon-png-2.png"
                    />
                  </a>
                </BackSide>
              </Flippy>

              <Flippy
                flipOnHover={true} // default false
                flipOnClick={false} // default false
                flipDirection="horizontal" // horizontal or vertical
                ref={(r) => (this.flippy = r)} // to use toggle method like this.flippy.toggle()
                // if you pass isFlipped prop component will be controlled component.
                // and other props, which will go to div
                style={{ width: "300px", height: "300px" }} /// these are optional style, it is not necessary
              >
                <FrontSide
                  style={{
                    backgroundColor: "#3a3a3c",
                    display: "flex",
                    alignItems: "center",
                    flexDirection: "column",
                  }}
                >
                  <img
                    width="250px"
                    height="250px"
                    src="https://cdn3.iconfinder.com/data/icons/contact-us-outline-3/64/14-Email-512.png"
                  />
                </FrontSide>
                <BackSide
                  style={{
                    backgroundColor: "#3385df",
                    display: "flex",
                    alignItems: "center",
                    flexDirection: "column",
                  }}
                >
                  <h2>Contacteaza-ne</h2>
                  <p className="App">
                    {" "}
                    Informatii legate de adresa cabinetului, email si numar de
                    telefon
                  </p>
                  <a href="http://localhost:3000/contact">
                    <img
                      width="120px"
                      height="120px"
                      src="https://ya-webdesign.com/transparent250_/mouse-click-icon-png-2.png"
                    />
                  </a>
                </BackSide>
              </Flippy>
            </div>
          </Container>
        </Jumbotron>

        <ScrollUpButton
          style={{
            backgroundColor: "#3385df",
          }}
        />
      </div>
    );
  }
}

export default Home;
