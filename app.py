import os
from flask import Flask, render_template, request, redirect
from flask_mail import Mail, Message
from flask_cors import CORS, cross_origin

mail = Mail()

app = Flask(__name__)

SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY
CORS(app)

app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'noreply.cabinetofta@gmail.com'
app.config['MAIL_PASSWORD'] = 'Cabinet.12345'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True

mail.init_app(app)


	
@app.route('/result', methods = ['POST'])
def result():
    if request.form['email'] is None:        
        return "No email information is given" 
    sendMail(request.form['email'])
    return redirect("http://localhost:3000/despre", code=302)

def sendMail(to):
    recipient = to;
    msg = Message("OftaReview - Informatii Suplimentare", sender = app.config.get("MAIL_USERNAME"),
            recipients = [recipient],
            body= "Centrul de investigaţii oftalmologice OftaReview Cluj pune la dispoziţie pacienţilor săi încă o posibilitate de tratament oftalmologic inovator şi eficient. Tratamentul se adresează persoanelor cu miopie şi/sau astigmatism şi constă în utilizarea lentilelor de ortokeratologie, cunoscute şi sub titulatura de lentile rigide de noapte (sau, pe scurt, OKT)."
    )  
    mail.send(msg)

if __name__ == "__main__":
    app.run(debug = True)