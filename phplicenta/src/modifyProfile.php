 <?php   
header("Access-Control-Allow-Origin: http://localhost:3000");
header("Access-Control-Allow-Credentials:true");
header('Content-type: application/json');  
session_start();
require_once("../src/config.php");
require_once("../src/manager.php"); 
 
        
$json = file_get_contents('php://input');
$obj = json_decode($json,true);

$nume_prenume = $obj['nume_prenume'];
$data_nasterii = $obj['data_nasterii'];
$CNP = $obj['CNP'];
$varsta = $obj['varsta']; 
$adresa = $obj['adresa']; 
$telefon = $obj['telefon']; 
$email = $obj['email'];

$db = connectBase();  

$manager = new manager($db); 
$bool = $manager->modifyProfile($nume_prenume, $data_nasterii, $CNP, $varsta, $adresa, $telefon,$email); 

 
if($bool)
{
    $msg = "Update realizat cu succes";
} 
else
{
    $msg = 'Update-ul nu s-a realizat cu succes';
}
    
$msgJson = json_encode($msg); 
echo $msgJson;

 ?>

 