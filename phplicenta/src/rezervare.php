<?php
class rezervare
{
	public $idrezervare;
	public $start;
	public $motiv;
	public $username;

	 
	public function __construct(  $itms ) {
	    foreach( $itms as $name => $val )
	        $this->$name=$val;
	}

	public function getIdRezervare(){
		return $this->idrezervare;
	}

	public function getStart(){
		return $this->start;
	}

	public function getMotiv()
	{
	return $this->motiv;
	}

	public function getUsername()
	{
	return $this->username;
	}


	public function setIdRezervare($idrezervare){
		return $this->idrezervare = $idrezervare;
	}

	public function setStart($start){
		return $this->start = $start;
	}

	public function setMotiv($motiv)
	{
		$this->motiv = $motiv;
	}

	public function setUsername($username)
	{
		$this->username = $username;
	}

}
?>  