<?php 
header("Access-Control-Allow-Origin: http://localhost:3000");  
header("Access-Control-Allow-Credentials:true");
header('Content-type: application/json');  
session_start();
require_once("../src/config.php");
require_once("../src/manager.php"); 

$json = file_get_contents('php://input'); 
$_SESSION = array();

if(isset($_COOKIE[session_name()]))
{
	setcookie(session_name(),'',time()-3500,'/');
}

session_unset();
session_destroy();   

$msg = "delogat";
$msgJson = json_encode($msg);
echo($msgJson);

?>