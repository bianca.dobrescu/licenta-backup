<?php
class pacient
{
	private $idpacient;
	private $nume_prenume; 
	private $data_nasterii;
	private $CNP; 
	private $varsta;
	private $adresa; 
	private $telefon;
	private $email;

	 
	public function __construct(  $itms ) {
	    foreach( $itms as $name => $val )
	        $this->$name=$val;
	}

	public function getIdPacient()
	{
	return $this->idpacient;
	}

	 
	public function getNumePrenume()
	{
		return $this->nume_prenume;
	}

	public function getDataNasterii()
	{
	return $this->data_nasterii;
	}

	 
	public function getCNP()
	{
		return $this->CNP;
	}

	public function getVarsta()
	{
	return $this->varsta;
	}
	 
	public function getAdresa()
	{
		return $this->adresa;
	}

	public function getTelefon()
	{
		return $this->telefon;
	}

	public function getEmail()
	{
		return $this->email;
	}


	public function setIdPacient($idpacient)
	{
		$this->idpacient = $idpacient;
	}  

	public function setNumePrenume($nume)
	{
		$this->nume_prenume = $nume;
	}

	public function setDataNasterii($dn)
	{
		$this->data_nasterii = $dn;
	}  

	public function setCNP($cnp)
	{
		$this->CNP = $cnp;
	}

	public function setVarsta($varsta)
	{
		$this->varsta = $varsta;
	}  

	public function setAdresa($adr)
	{
		$this->adresa = $adr;
	}

	public function setTelefon($telefon)
	{
		$this->telefon = $telefon;
	}

	public function setEmail($email)
	{
		$this->email = $email;
	}

}
?>  