<?php
header("Access-Control-Allow-Origin: http://localhost:3000");
header("Access-Control-Allow-Credentials:true");
header('Content-type: application/json');  
session_start();
require_once("../src/config.php");
require_once("../src/manager.php"); 
 
        

    $json = file_get_contents('php://input');
     
    $obj = json_decode($json,true);

$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "licenta";

$date = $obj['date'];
    $time = $obj['time'];
    $motiv = $obj['motiv']; 
    $start = $date . " " . $time;


$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$username = $_SESSION['username'];
$sql = "SELECT idrezervare, start, motiv, username FROM rezervare WHERE start ='{$start}'";
$result = $conn->query($sql);

$json = mysqli_fetch_all ($result, MYSQLI_ASSOC);
if ($result->num_rows > 0) {
  echo json_encode('Deja exista o programare in ziua si la ora selectata');
} else {
  $id = rand(10,100000);
  $sql = "INSERT INTO rezervare (idrezervare, start, motiv, username) VALUES ({$id}, '{$start}', '{$motiv}', '{$username}')";     
  $result = $conn->query($sql);

  if (!mysqli_query($conn,$sql)) {
    echo json_encode('Programare creata cu succes');
  } else {
    echo json_encode('Esec');
  }
}
$conn->close();
?>