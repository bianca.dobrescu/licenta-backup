 <?php   
header("Access-Control-Allow-Origin: http://localhost:3000");
header("Access-Control-Allow-Credentials:true");
header('Content-type: application/json');  
session_start();
require_once("../src/config.php");
require_once("../src/manager.php"); 
 
        
$username = $_SESSION['username'];
$db = connectBase();  

$manager = new manager($db); 
$pacient = $manager->getUserData($username); 
$nume = $pacient->getNumePrenume();
$data = $pacient->getDataNasterii();
$cnp = $pacient->getCNP();
$varsta = $pacient->getVarsta();
$adresa = $pacient->getAdresa();
$telefon = $pacient->getTelefon();
$email = $pacient->getEmail();

 
if(isset($pacient) and ($pacient instanceof Pacient))
{
    $msg = $pacient;
} 
else 
{
    $msg = 'Username sau parola invalida';
}
    
    $msgJson = json_encode($nume . "," . $data . "," . $cnp . "," . $varsta . "," . $adresa . "," . $telefon . "," . $email); 
     
    
    echo $msgJson;

 ?>