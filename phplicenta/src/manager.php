<?php 
  error_reporting(E_ALL);
  require_once("../src/user.php"); 
  require_once("../src/pacient.php"); 


class manager
{
  private $_db;

  public function __construct($db)
  {
    $this->setDb($db);
  }

  public function addRezervare(rezervare $r)
  {
    $q = $this->_db->prepare('INSERT INTO rezervare SET idrezervare = :idrezervare, start_time = :start, motiv = :motiv, username = :username');  
    $q->bindValue(':idrezervare', $r->getIdRezervare());
    $q->bindValue(':start', $r->getStart());  
    $q->bindValue(':motiv', $r->getMotiv());  
    $q->bindValue(':username', $r->getUsername()); 
    $q->execute(); 
  }
  
  
  public function getUser($username, $parola)
  {  
      if ($parola == 'dont_take_it'){
          $q = $this->_db->query("SELECT username, parola FROM userpacient WHERE username ='{$username}'");
      }
      else{
         // $password = sha1($password);
          $q = $this->_db->query("SELECT username, parola FROM userpacient WHERE username ='{$username}' AND parola = '{$parola}'"); 
      }

      $donnees = $q->fetch(PDO::FETCH_ASSOC);    
      if (is_null($donnees) || $donnees == false )
      { 
          return false; 
      }   
      else 
          return new User($donnees);
  }

  public function getUserData($id)
  {
    $username = $_SESSION['username'];
    $q = $this->_db->query("SELECT idpacient, nume_prenume, data_nasterii, CNP, varsta, adresa, telefon, email FROM pacient WHERE idu ='{$username}'");
    $donnees = $q->fetch(PDO::FETCH_ASSOC);    
      if (is_null($donnees) || $donnees == false )
      { 
          return false; 
      }   
      else 
          return new Pacient($donnees);
  }

    public function getRezervare($start)
  {  
      
      $q = $this->_db->query("SELECT idrezervare, start, motiv, username FROM rezervare WHERE start ='{$start}'"); 
      
      $donnees = $q->fetch(PDO::FETCH_ASSOC);    
      if (is_null($donnees) || $donnees == false )
      { 
          return false; 
      }   
      else 
          return new Rezervare($donnees);
  }

  public function modifyProfile($nume_prenume,$data_nasterii,$CNP,$varsta,$adresa,$telefon,$email)
  {
    $username = $_SESSION['username'];
    $q = $this->_db->query("UPDATE pacient
          SET nume_prenume = '{$nume_prenume}', data_nasterii = '{$data_nasterii}', CNP = {$CNP}, varsta = {$varsta}, adresa = '{$adresa}', telefon = {$telefon}, email = '{$email}'
          WHERE idu='{$username}'");
      if ($q)
      { 
          return true; 
      }   
      else 
          return false;
  }

 
   
  public function setDb(PDO $db)
  {
    $this->_db = $db;
  }
}
?> 