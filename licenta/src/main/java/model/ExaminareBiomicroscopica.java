package model;

public class ExaminareBiomicroscopica {
	private int idExaminareBiomicroscopica;
	private int idExaminareMedic;
	private String conjunctiva;
	private String cornee;
	private String pupile;
	private String cristalin;
	private String orbita;
	private String aparat;

	public ExaminareBiomicroscopica (int idExaminareBiomicroscopica, int idem, String conjunctiva, String cornee, String pupile, 
			String cristalin, String orbita, String aparat) {
		setIdExaminareBiomicroscopica(idExaminareBiomicroscopica);
		setIdExaminareMedic(idem);
		setConjunctiva(conjunctiva);
		setCornee(cornee);
		setPupile(pupile);
		setCristalin(cristalin);
		setOrbita(orbita);
		setAparat(aparat);
	}
	
	public void setIdExaminareBiomicroscopica(int id) {
		this.idExaminareBiomicroscopica = id;
	}
	
	public void setIdExaminareMedic(int id) {
		this.idExaminareMedic = id;
	}
	
	public void setConjunctiva(String c) {
		this.conjunctiva = c;
	}
	
	public void setCornee(String c) {
		this.cornee = c;
	}
	
	public void setPupile(String p) {
		this.pupile = p;
	}
	
	public void setCristalin(String c) {
		this.cristalin = c;
	}
	
	public void setOrbita(String o) {
		this.orbita = o;
	}
	
	public void setAparat(String s) {
		this.aparat = s;
	}
	
	public int getIdExaminareBiomicroscopica() {
		return idExaminareBiomicroscopica;
	}
	
	public int getIdExaminareMedic() {
		return idExaminareMedic;
	}
	
	public String getConjunctiva() {
		return conjunctiva;
	}
	
	public String getCornee() {
		return cornee;
	}
	
	public String getPupile() {
		return pupile;
	}
	
	public String getCristalin() {
		return cristalin;
	}
	
	public String getOrbita() {
		return orbita;
	}
	
	public String getAparat() {
		return aparat;
	}

}
