package model;

public class Diagnostic {
	private int idDiagnostic;
	private String numeMaladie;

	public Diagnostic (int idDiagnostic, String numeMaladie) {
		setIdDiagnostic(idDiagnostic);
		setNumeMaladie(numeMaladie);
	}
	
	public void setIdDiagnostic(int id) {
		this.idDiagnostic = id;
	}
	
	public void setNumeMaladie(String nm) {
		this.numeMaladie = nm;
	}
	
	public int getIdDiagnostic() {
		return idDiagnostic;
	}
	
	public String getNumeMaladie() {
		return numeMaladie;
	}
}
