package model;

public class Anamneza {
	private int idAnamneza;
	private int idPacient;
	private String factoriRisc;
	private String tratamentEfect;
	private String examenLaboratorValoriNormale;
	private String examenLaboratorValoriPatologice;
	private String antecedenteOculare;
	private String examinariParaclinice;

	public Anamneza (int idAnamneza, int idPacient, String factoriRisc, String tratamentEfect, 
			String elvn, String elvp, String ao, String ep) {
		setIdAnamneza(idAnamneza);
		setIdPacient(idPacient);
		setFactoriRisc(factoriRisc);
		setTratamentEfect(tratamentEfect);
		setELVN(elvn);
		setELVP(elvp);
		setAntecedenteOculare(ao);
		setEP(ep);
	}
	
	public void setIdAnamneza(int id) {
		this.idAnamneza = id;
	}
	
	public void setIdPacient(int id) {
		this.idPacient = id;
	}
	
	public void setFactoriRisc(String fr) {
		this.factoriRisc = fr;
	}
	
	public void setTratamentEfect(String te) {
		this.tratamentEfect = te;
	}
	
	public void setELVN(String elvn) {
		this.examenLaboratorValoriNormale = elvn;
	}
	
	public void setELVP(String elvp) {
		this.examenLaboratorValoriPatologice = elvp;
	}
	
	public void setAntecedenteOculare(String a) {
		this.antecedenteOculare = a;
	}
	
	public void setEP(String ep) {
		this.examinariParaclinice = ep;
	}
	
	public int getIdAnamneza() {
		return idAnamneza;
	}
	
	public int getIdPacient() {
		return idPacient;
	}
	
	public String getFactoriRisc() {
		return factoriRisc;
	}
	
	public String getTratamentEfect() {
		return tratamentEfect;
	}
	
	public String getELVN() {
		return examenLaboratorValoriNormale;
	}
	
	public String getELVP() {
		return examenLaboratorValoriPatologice;
	}
	
	public String getAntecedenteOculare() {
		return antecedenteOculare;
	}
	
	public String getEP() {
		return examinariParaclinice;
	}
}
