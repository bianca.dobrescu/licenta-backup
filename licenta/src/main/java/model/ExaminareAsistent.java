package model;

public class ExaminareAsistent {
	private int idExaminareAsistent;
	private int idPacient;
	private String vedereAVOD;
	private String vedereAVOS;
	private String tensiuneIntraoculara;
	private String refractiaFaraTropicamidaOD;
	private String refractiaFaraTropicamidaOS;
	private String refractiaCuTropicamidaOD;
	private String refractiaCuTropicamidaOS;

	public ExaminareAsistent (int idExaminareAsistent, int idPacient, String vavod, String vavos, String ti, 
			String rftod, String rftos, String rctod, String rctos) {
		setIdExaminareAsistent(idExaminareAsistent);
		setIdPacient(idPacient);
		setVedereAVOD(vavod);
		setVedereAVOS(vavos);
		setTensiuneIntraoculara(ti);
		setRFTOD(rftod);
		setRFTOS(rftos);
		setRCTOD(rctod);
		setRCTOS(rctos);
	}
	
	public void setIdExaminareAsistent(int id) {
		this.idExaminareAsistent = id;
	}
	
	public void setIdPacient(int id) {
		this.idPacient = id;
	}
	
	public void setVedereAVOD(String vavod) {
		this.vedereAVOD = vavod;
	}
	
	public void setVedereAVOS(String vavos) {
		this.vedereAVOS = vavos;
	}
	
	public void setTensiuneIntraoculara(String ti) {
		this.tensiuneIntraoculara = ti;
	}
	
	public void setRFTOD(String rftod) {
		this.refractiaFaraTropicamidaOD = rftod;
	}
	
	public void setRFTOS(String rftos) {
		this.refractiaFaraTropicamidaOS = rftos;
	}
	
	public void setRCTOD(String rctod) {
		this.refractiaCuTropicamidaOD = rctod;
	}
	
	public void setRCTOS(String rctos) {
		this.refractiaCuTropicamidaOS = rctos;
	}
	
	public int getIdExaminareAsistent() {
		return idExaminareAsistent;
	}
	
	public int getIdPacient() {
		return idPacient;
	}
	
	public String getVedereAVOD() {
		return vedereAVOD;
	}
	
	public String getVedereAVOS() {
		return vedereAVOS;
	}
	
	public String getTensiuneIntraoculara() {
		return tensiuneIntraoculara;
	}
	
	public String getRFTOD() {
		return refractiaFaraTropicamidaOD;
	}
	
	public String getRFTOS() {
		return refractiaFaraTropicamidaOS;
	}
	
	public String getRCTOD() {
		return refractiaCuTropicamidaOD;
	}
	
	public String getRCTOS() {
		return refractiaCuTropicamidaOS;
	}

}
