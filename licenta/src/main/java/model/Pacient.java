package model;

public class Pacient {
	private int idPacient;
	private String numePrenume;
	private String dataNasterii;
	private long CNP;
	private int varsta;
	private String adresa;
	private long telefon;
	private String email;
	private String username;

	public Pacient (int idPacient, String np, String dn, long cnp,
			int v, String a, long t, String email,String u) {
		setIdPacient(idPacient);
		setNumePrenume(np);
		setDataNasterii(dn);
		setCNP(cnp);
		setVarsta(v);
		setAdresa(a);
		setTelefon(t);
		setEmail(email);
		setUsername(u);
	}
	
	public void setIdPacient(int id) {
		this.idPacient = id;
	}
	
	public void setNumePrenume(String fr) {
		this.numePrenume = fr;
	}
	
	public void setDataNasterii(String te) {
		this.dataNasterii = te;
	}
	
	public void setCNP(long elvn) {
		this.CNP = elvn;
	}
	
	public void setVarsta(int elvp) {
		this.varsta = elvp;
	}
	
	public void setAdresa(String ep) {
		this.adresa = ep;
	}
	
	public void setTelefon(long ep) {
		this.telefon = ep;
	}
	
	public void setEmail(String e) {
		this.email = e;
	}
	
	public void setUsername(String u) {
		this.username = u;
	}
	
	public int getIdPacient() {
		return idPacient;
	}
	
	public String getNumePrenume() {
		return numePrenume;
	}
	
	public String getDataNasterii() {
		return dataNasterii;
	}
	
	public long getCNP() {
		return CNP;
	}
	
	public int getVarsta() {
		return varsta;
	}
	
	public String getAdresa() {
		return adresa;
	}
	
	public long getTelefon() {
		return telefon;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getUsername() {
		return username;
	}

}
