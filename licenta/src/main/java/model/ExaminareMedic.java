package model;

public class ExaminareMedic {
	private int idExaminareMedic;
	private int idPacient;
	private int idDiagnostic;
	private String mobilitateOculara;
	private String ct;
	private String stereoscopie;
	private String fundOchi;
	private String tratament;

	public ExaminareMedic (int idExaminareMedic, int idPacient, int idD, String mo, 
			String ct, String s, String fo, String t) {
		setIdExaminareMedic(idExaminareMedic);
		setIdPacient(idPacient);
		setIdDiagnostic(idD);
		setMobilitateOculara(mo);
		setCT(ct);
		setStereoscopie(s);
		setFundOchi(fo);
		setTratament(t);
	}
	
	public void setIdExaminareMedic(int id) {
		this.idExaminareMedic = id;
	}
	
	public void setIdPacient(int id) {
		this.idPacient = id;
	}
	
	public void setIdDiagnostic(int id) {
		this.idDiagnostic = id;
	}
	
	public void setMobilitateOculara(String vavod) {
		this.mobilitateOculara = vavod;
	}
	
	public void setCT(String vavos) {
		this.ct = vavos;
	}
	
	public void setStereoscopie(String ti) {
		this.stereoscopie = ti;
	}
	
	public void setFundOchi(String rftod) {
		this.fundOchi = rftod;
	}
	
	public void setTratament(String t) {
		this.tratament = t;
	}
	
	public int getIdExaminareMedic() {
		return idExaminareMedic;
	}
	
	public int getIdPacient() {
		return idPacient;
	}
	
	public int getIdDiagnostic() {
		return idDiagnostic;
	}
	
	public String getMobilitateOculara() {
		return mobilitateOculara;
	}
	
	public String getCT() {
		return ct;
	}
	
	public String getStereoscopie() {
		return stereoscopie;
	}
	
	public String getFundOchi() {
		return fundOchi;
	}
	
	public String getTratament() {
		return tratament;
	}

}
