package model;

public class User {
	private int idUser;
	private String username;
	private String parola;
	private String rol;
	private String contactiv;

	public User (int idUser, String u, String p, String r, String ca) {
		setIdUser(idUser);
		setUsername(u);
		setParola(p);
		setRol(r);
		setContActiv(ca);
	}
	
	public void setIdUser(int id) {
		this.idUser = id;
	}
	
	public void setUsername(String fr) {
		this.username = fr;
	}
	
	public void setParola(String te) {
		this.parola = te;
	}
	
	public void setRol(String elvn) {
		this.rol = elvn;
	}
	
	public void setContActiv(String ca) {
		this.contactiv = ca;
	}
	
	public int getIdUser() {
		return idUser;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getParola() {
		return parola;
	}
	
	public String getRol() {
		return rol;
	}
	
	public String getContActiv() {
		return contactiv;
	}

}
