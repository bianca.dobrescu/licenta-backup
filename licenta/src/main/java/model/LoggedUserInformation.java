package model;

public class LoggedUserInformation {
	private int loggedUserId;
	private String loggedUserRole;
	private int searchedPatientId;

	public LoggedUserInformation (int id1, String r, int id2) {
		setLoggedUserId(id1);
		setLoggedUserRole(r);
		setSearchedPatientId(id2);
	}
	
	public void setLoggedUserId(int id) {
		this.loggedUserId = id;
	}
	
	public void setLoggedUserRole(String r) {
		this.loggedUserRole = r;
	}
	
	public void setSearchedPatientId(int id) {
		this.searchedPatientId = id;
	}
	
	public int getLoggedUserId() {
		return loggedUserId;
	}
	
	public String getLoggedUserRole() {
		return loggedUserRole;
	}
	
	public int getSearchedPatientId() {
		return searchedPatientId;
	}

}
