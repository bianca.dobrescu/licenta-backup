package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Pacient;
import model.User;

public class UserDAO {
	protected static final Logger LOGGER = Logger.getLogger(UserDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO user (iduser,username, parola, rol, cont_activ)"
			+ " VALUES (?,?,?,?,?)";
	private static final String insertStatementString2 = "INSERT INTO userpacient (username, parola)"
			+ " VALUES (?,?)";
	private final static String findUsernameStatementString = "SELECT * FROM user WHERE username = ?";
	private final static String findIdUserStatementString = "SELECT * FROM user WHERE iduser = ?";
	private final static String getUsersStatementString ="SELECT * FROM user WHERE cont_activ = ?";
	private final static String updateUserStatementString ="UPDATE user SET cont_activ=? WHERE iduser=?";

	
	public static User findByUsername(String username){
		User toReturn= null;

		Connection dbConnection = (Connection) DatabaseConnection.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;

		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(findUsernameStatementString);
			findStatement.setNString(1, username);
			rs = findStatement.executeQuery();
			rs.next();
			int id = rs.getInt("iduser");
			String u = rs.getString("username");
			String parola = rs.getString("parola");
			String rol = rs.getString("rol");
			String contaprobat = rs.getString("cont_activ");

			toReturn = new User(id,username,parola,rol, contaprobat);

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"UserDAO:findByUsername " + e.getMessage());
		} finally {
			DatabaseConnection.close(rs);
			DatabaseConnection.close(findStatement);
			DatabaseConnection.close(dbConnection);
		}
		return toReturn;
	}
	
	public static User findByIdUser(int id){
		User toReturn= null;

		Connection dbConnection = (Connection) DatabaseConnection.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;

		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(findIdUserStatementString);
			findStatement.setInt(1, id);
			rs = findStatement.executeQuery();
			rs.next();
			String username = rs.getString("username");
			String parola = rs.getString("parola");
			String rol = rs.getString("rol");
			String contaprobat = rs.getString("cont_activ");

			toReturn = new User(id,username,parola,rol,contaprobat);

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"UserDAO:findByIdUser " + e.getMessage());
		} finally {
			DatabaseConnection.close(rs);
			DatabaseConnection.close(findStatement);
			DatabaseConnection.close(dbConnection);
		}
		return toReturn;
	}

	public static int addUser(User u) {
		Connection dbConnection = DatabaseConnection.getConnection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;

		try {
			insertStatement = (PreparedStatement) dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, u.getIdUser());
			insertStatement.setString(2, u.getUsername());
			insertStatement.setString(3, u.getParola());
			insertStatement.setString(4, u.getRol());
			insertStatement.setString(5, u.getContActiv());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "UserDAO:addUser " + e.getMessage());
		} finally {
			DatabaseConnection.close(insertStatement);
			DatabaseConnection.close(dbConnection);
		}
		return insertedId;
	}
	
	public static int addUserPacient(String username, String parola) {
		Connection dbConnection = DatabaseConnection.getConnection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;

		try {
			insertStatement = (PreparedStatement) dbConnection.prepareStatement(insertStatementString2, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, username);
			insertStatement.setString(2, parola);
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "UserDAO:addUserPacient " + e.getMessage());
		} finally {
			DatabaseConnection.close(insertStatement);
			DatabaseConnection.close(dbConnection);
		}
		return insertedId;
	}
	
	public static ArrayList<User> getUsersPending(){
		Connection dbConnection = DatabaseConnection.getConnection();
		PreparedStatement selectStatement = null;
		ResultSet rs = null;
		ArrayList<User> users= new ArrayList<User>();

		try{
			selectStatement = (PreparedStatement) dbConnection.prepareStatement(getUsersStatementString);
			selectStatement.setString(1, "Pending");
			rs = selectStatement.executeQuery();

			while(rs.next()){
				int id= rs.getInt("iduser");
				String username = rs.getString("username");
				String parola = rs.getString("parola");
				String rol = rs.getString("rol");
				String contactiv = rs.getString("cont_activ");
				User u = new User(id,username,parola,rol,contactiv);
				users.add(u);
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "UserDAO:getUsersPending " + e.getMessage());
		} finally {
			DatabaseConnection.close(selectStatement);
			DatabaseConnection.close(dbConnection);
		}
		return users;
	}
	
	public static int updateUser(int idu, String s){
		Connection dbConnection = DatabaseConnection.getConnection();
		PreparedStatement updateStatement = null;
		int toReturn=-1;

		try{
			updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateUserStatementString);
             
			updateStatement.setString(1,s);
			updateStatement.setInt(2,idu);

			toReturn=updateStatement.executeUpdate();

		} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "UserDAO:updateUser " + e.getMessage());
		} finally {
			DatabaseConnection.close(updateStatement);
			DatabaseConnection.close(dbConnection);
		}

		return toReturn;
	}

}
