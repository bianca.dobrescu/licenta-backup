package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Pacient;
import model.User;

public class PacientDAO {
	protected static final Logger LOGGER = Logger.getLogger(PacientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO pacient (idpacient, nume_prenume, data_nasterii, CNP, varsta, adresa, telefon, email,idu)"
			+ " VALUES (?,?,?,?,?,?,?,?,?)";
	private final static String findIdPacientStatementString = "SELECT * FROM pacient WHERE idpacient = ?";
	private final static String getPatientsStatementString ="SELECT * FROM pacient ORDER BY nume_prenume ASC";
	

	public static int addPacient(Pacient u) {
		Connection dbConnection = DatabaseConnection.getConnection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;

		try {
			insertStatement = (PreparedStatement) dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, u.getIdPacient());
			insertStatement.setString(2, u.getNumePrenume());
			insertStatement.setString(3, u.getDataNasterii());
			insertStatement.setLong(4, u.getCNP());
			insertStatement.setInt(5, u.getVarsta());
			insertStatement.setString(6, u.getAdresa());
			insertStatement.setLong(7, u.getTelefon());
			insertStatement.setString(8, u.getEmail());
			insertStatement.setString(9, u.getUsername());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "PacientDAO:addPacient " + e.getMessage());
		} finally {
			DatabaseConnection.close(insertStatement);
			DatabaseConnection.close(dbConnection);
		}
		return insertedId;
	}
	
	public static ArrayList<Pacient> getPatients(){
		Connection dbConnection = DatabaseConnection.getConnection();
		PreparedStatement selectStatement = null;
		ResultSet rs = null;
		ArrayList<Pacient> pacients= new ArrayList<Pacient>();

		try{
			selectStatement = (PreparedStatement) dbConnection.prepareStatement(getPatientsStatementString);
			rs = selectStatement.executeQuery();

			while(rs.next()){
				int id= rs.getInt("idpacient");
				String numePrenume = rs.getString("nume_prenume");
				String dataNasteriiString = rs.getString("data_nasterii");
				//DateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);
				//Date dataNasterii = format.parse(dataNasteriiString);
				long cnp = rs.getLong("CNP");
				int varsta = rs.getInt("varsta");
				String adresa = rs.getString("adresa");
				long telefon = rs.getLong("telefon");
				String email = rs.getString("email");
				String username = rs.getString("idu");
				Pacient p= new Pacient(id,numePrenume,dataNasteriiString,cnp,varsta,adresa,telefon,email,username);
				pacients.add(p);
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "PacientDAO:getPatients " + e.getMessage());
		} finally {
			DatabaseConnection.close(selectStatement);
			DatabaseConnection.close(dbConnection);
		}
		return pacients;
	}

}
