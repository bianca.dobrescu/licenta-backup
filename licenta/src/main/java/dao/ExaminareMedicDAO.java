package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Anamneza;
import model.ExaminareMedic;
import model.Pacient;

public class ExaminareMedicDAO {
	
	protected static final Logger LOGGER = Logger.getLogger(ExaminareMedicDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO examinaremedic (idexaminaremedic, mobilitate_oculara, CT, stereoscopie,"
			+ "fund_ochi, tratament, idpacient, iddiagnostic)"
			+ " VALUES (?,?,?,?,?,?,?,?)";
	private final static String getExaminareMedicStatementString = "SELECT * FROM examinaremedic WHERE idpacient = ?";
	private final static String updateMobilitateStatementString ="UPDATE examinaremedic SET mobilitate_oculara=? WHERE idpacient=?";
	private final static String updateCTStatementString ="UPDATE examinaremedic SET CT=? WHERE idpacient=?";
	private final static String updateStereoscopieStatementString ="UPDATE examinaremedic SET stereoscopie=? WHERE idpacient=?";
	private final static String updateFundOchiStatementString ="UPDATE examinaremedic SET fund_ochi=? WHERE idpacient=?";
	private final static String updateDiagnosticStatementString ="UPDATE examinaremedic SET iddiagnostic=? WHERE idpacient=?";
	private final static String updateTratamentStatementString ="UPDATE examinaremedic SET tratament=? WHERE idpacient=?";

	public static int addExaminareMedic(ExaminareMedic u) {
		Connection dbConnection = DatabaseConnection.getConnection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;

		try {
			insertStatement = (PreparedStatement) dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, u.getIdExaminareMedic());
			insertStatement.setString(2, u.getMobilitateOculara());
			insertStatement.setString(3, u.getCT());
			insertStatement.setString(4, u.getStereoscopie());
			insertStatement.setString(5, u.getFundOchi());
			insertStatement.setString(6, u.getTratament());
			insertStatement.setInt(7, u.getIdPacient());
			insertStatement.setInt(8, u.getIdDiagnostic());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ExaminareMedicDAO:addExaminareMedic " + e.getMessage());
		} finally {
			DatabaseConnection.close(insertStatement);
			DatabaseConnection.close(dbConnection);
		}
		return insertedId;
	}
	
	public static ExaminareMedic findByIdPacient(int idpacient){
		ExaminareMedic toReturn= null;

		Connection dbConnection = (Connection) DatabaseConnection.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;

		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(getExaminareMedicStatementString);
			findStatement.setLong(1, idpacient);
			rs = findStatement.executeQuery();
			//rs.next();
			
			if (rs.next()) {
				int idem = rs.getInt("idexaminaremedic");
				String mo = rs.getString("mobilitate_oculara");
				String ct = rs.getString("CT");
				String st = rs.getString("stereoscopie");
				String fo = rs.getString("fund_ochi");
				String t = rs.getString("tratament");
				int idd = rs.getInt("iddiagnostic");

				toReturn = new ExaminareMedic(idem,idpacient, idd, mo, ct, st, fo, t);
	        }

			

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ExaminareMedicDAO:findByIdPacient " + e.getMessage());
		} finally {
			if (rs != null)
			       DatabaseConnection.close(rs);
			DatabaseConnection.close(findStatement);
			DatabaseConnection.close(dbConnection);
		}
		return toReturn;
	}
	
	public static int updateExaminareMedic(int idp, String s, int nr){
		Connection dbConnection = DatabaseConnection.getConnection();
		PreparedStatement updateStatement = null;
		int toReturn=-1;

		try{
			switch(nr) {
			   case 1:  updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateMobilitateStatementString);
                  break;
               case 2:  updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateCTStatementString);
                  break;
               case 3:  updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateStereoscopieStatementString);
                  break;
               case 4:  updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateFundOchiStatementString);
                  break;
               case 5:  updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateDiagnosticStatementString);
                  break;
               case 6:  updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateTratamentStatementString);
                  break;
               default: updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateTratamentStatementString);
                  break;
			}
				
			
			updateStatement.setString(1,s);
			updateStatement.setInt(2,idp);

			toReturn=updateStatement.executeUpdate();

		} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "ExaminareMedicDAO:updateExaminareMedic " + e.getMessage());
		} finally {
			DatabaseConnection.close(updateStatement);
			DatabaseConnection.close(dbConnection);
		}

		return toReturn;
	}
	
	public static int updateDiagnosticExaminareMedic(int idp, int s, int nr){
		Connection dbConnection = DatabaseConnection.getConnection();
		PreparedStatement updateStatement = null;
		int toReturn=-1;

		try{
			updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateDiagnosticStatementString);
             
			updateStatement.setInt(1,s);
			updateStatement.setInt(2,idp);

			toReturn=updateStatement.executeUpdate();

		} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "ExaminareMedicDAO:updateDiagnosticExaminareMedic " + e.getMessage());
		} finally {
			DatabaseConnection.close(updateStatement);
			DatabaseConnection.close(dbConnection);
		}

		return toReturn;
	}
	


}
