package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.ExaminareAsistent;
import model.ExaminareMedic;

public class ExaminareAsistentDAO {
	protected static final Logger LOGGER = Logger.getLogger(ExaminareMedicDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO examinareasistent (idexaminareasistent, vederea_aw_od, vederea_aw_os, tensiune_intraoculara, refractia_fara_tropicamida_OD,"
			+ "refractia_fara_tropicamida_OS, refractia_cu_tropicamida_OD, refractia_cu_tropicamida_OS, idpacient)"
			+ " VALUES (?,?,?,?,?,?,?,?,?)";
	private final static String getExaminareAsistentStatementString = "SELECT * FROM examinareasistent WHERE idpacient = ?";
	private final static String updateVedereODStatementString ="UPDATE examinareasistent SET vederea_aw_od=? WHERE idpacient=?";
	private final static String updateVedereOSStatementString ="UPDATE examinareasistent SET vederea_aw_os=? WHERE idpacient=?";
	private final static String updateTensiuneStatementString ="UPDATE examinareasistent SET tensiune_intraoculara=? WHERE idpacient=?";
	private final static String updateRFTODStatementString ="UPDATE examinareasistent SET refractia_fara_tropicamida_OD=? WHERE idpacient=?";
	private final static String updateRFTOSStatementString ="UPDATE examinareasistent SET refractia_fara_tropicamida_OS=? WHERE idpacient=?";
	private final static String updateRCTODStatementString ="UPDATE examinareasistent SET refractia_cu_tropicamida_OD=? WHERE idpacient=?";
	private final static String updateRCTOSStatementString ="UPDATE examinareasistent SET refractia_cu_tropicamida_OS=? WHERE idpacient=?";
	
	public static int addExaminareAsistent(ExaminareAsistent u) {
		Connection dbConnection = DatabaseConnection.getConnection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;

		try {
			insertStatement = (PreparedStatement) dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, u.getIdExaminareAsistent());
			insertStatement.setString(2, u.getVedereAVOD());
			insertStatement.setString(3, u.getVedereAVOS());
			insertStatement.setString(4, u.getTensiuneIntraoculara());
			insertStatement.setString(5, u.getRFTOD());
			insertStatement.setString(6, u.getRFTOS());
			insertStatement.setString(7, u.getRCTOD());
			insertStatement.setString(8, u.getRCTOS());
			insertStatement.setInt(9, u.getIdPacient());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ExaminareAsistentDAO:addExaminareAsistent " + e.getMessage());
		} finally {
			DatabaseConnection.close(insertStatement);
			DatabaseConnection.close(dbConnection);
		}
		return insertedId;
	}
	
	public static ExaminareAsistent findByIdPacient(int idpacient){
		ExaminareAsistent toReturn= null;

		Connection dbConnection = (Connection) DatabaseConnection.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;

		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(getExaminareAsistentStatementString);
			findStatement.setLong(1, idpacient);
			rs = findStatement.executeQuery();
			//rs.next();
			
			if (rs.next()) {
				int idea = rs.getInt("idexaminareasistent");
				String aw_od = rs.getString("vederea_aw_od");
				String aw_os = rs.getString("vederea_aw_os");
				String ti = rs.getString("tensiune_intraoculara");
				String rftod = rs.getString("refractia_fara_tropicamida_OD");
				String rftos = rs.getString("refractia_fara_tropicamida_OS");
				String rctod = rs.getString("refractia_cu_tropicamida_OD");
				String rctos = rs.getString("refractia_cu_tropicamida_OS");
				int idp = rs.getInt("idpacient");

				toReturn = new ExaminareAsistent(idea,idpacient,aw_od,aw_os,ti,rftod,rftos,rctod,rctos);
	        }

			

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ExaminareAsistentDAO:findByIdPacient " + e.getMessage());
		} finally {
			if (rs != null)
			       DatabaseConnection.close(rs);
			DatabaseConnection.close(findStatement);
			DatabaseConnection.close(dbConnection);
		}
		return toReturn;
	}
	
	public static int updateExaminareAsistent(int idp, String s, int nr){
		Connection dbConnection = DatabaseConnection.getConnection();
		PreparedStatement updateStatement = null;
		int toReturn=-1;

		try{
			switch(nr) {
			   case 1:  updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateVedereODStatementString);
                  break;
               case 2:  updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateVedereOSStatementString);
                  break;
               case 3:  updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateTensiuneStatementString);
                  break;
               case 4:  updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateRFTODStatementString);
                  break;
               case 5:  updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateRFTOSStatementString);
                  break;
               case 6:  updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateRCTODStatementString);
                  break;
               default: updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateRCTOSStatementString);
                  break;
			}
				
			
			updateStatement.setString(1,s);
			updateStatement.setInt(2,idp);

			toReturn=updateStatement.executeUpdate();

		} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "ExaminareAsistentDAO:updateExaminareAsistent " + e.getMessage());
		} finally {
			DatabaseConnection.close(updateStatement);
			DatabaseConnection.close(dbConnection);
		}

		return toReturn;
	}
}
