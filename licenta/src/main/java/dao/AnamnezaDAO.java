package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Anamneza;
import model.Diagnostic;
import model.Pacient;

public class AnamnezaDAO {
	protected static final Logger LOGGER = Logger.getLogger(AnamnezaDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO anamneza (idanamneza, factori_risc, tratament_efect, examen_laborator_valori_normale,"
			+ "examen_laborator_valori_patologice, examinari_paraclinice, antecedente_oculare, idpacient)"
			+ " VALUES (?,?,?,?,?,?,?,?)";
	private final static String getAnamnezaStatementString = "SELECT * FROM anamneza WHERE idpacient = ?";
	private final static String updateStatementString ="UPDATE anamneza SET factori_risc=?,tratament_efect=?,examen_laborator_valori_normale=?,"
			+ "examen_laborator_valori_patologice=?,examinari_paraclinice=?,antecedente_oculare=? WHERE idpacient=?";
	

	public static int addAnamneza(Anamneza u) {
		Connection dbConnection = DatabaseConnection.getConnection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;

		try {
			insertStatement = (PreparedStatement) dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, u.getIdAnamneza());
			insertStatement.setString(2, u.getFactoriRisc());
			insertStatement.setString(3, u.getTratamentEfect());
			insertStatement.setString(4, u.getELVN());
			insertStatement.setString(5, u.getELVP());
			insertStatement.setString(6, u.getEP());
			insertStatement.setString(7, u.getAntecedenteOculare());
			insertStatement.setInt(8, u.getIdPacient());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "AnamnezaDAO:addAnamneza " + e.getMessage());
		} finally {
			DatabaseConnection.close(insertStatement);
			DatabaseConnection.close(dbConnection);
		}
		return insertedId;
	}
	
	public static Anamneza findByIdPacient(int idpacient){
		Anamneza toReturn= null;

		Connection dbConnection = (Connection) DatabaseConnection.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;

		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(getAnamnezaStatementString);
			findStatement.setLong(1, idpacient);
			rs = findStatement.executeQuery();
			//rs.next();
			if (rs.next()) {
				String fr = rs.getString("factori_risc");
				String te = rs.getString("tratament_efect");
				String elvn = rs.getString("examen_laborator_valori_normale");
				String elvp = rs.getString("examen_laborator_valori_patologice");
				String ep = rs.getString("examinari_paraclinice");
				String a = rs.getString("antecedente_oculare");
				int ida = rs.getInt("idanamneza");

				toReturn = new Anamneza(ida,idpacient,fr,te,elvn,elvp,a,ep);
	        }

		

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"AnamnezaDAO:findByIdPacient " + e.getMessage());
		} finally {
		    if (rs != null)
		       DatabaseConnection.close(rs);
		    
		    DatabaseConnection.close(findStatement);
		    DatabaseConnection.close(dbConnection);
		}
		return toReturn;
	}
	
	public static int updateAnamneza(int id, String fr, String te, String elvn, String elvp, String ep, String a){
		Connection dbConnection = DatabaseConnection.getConnection();
		PreparedStatement updateStatement = null;
		int toReturn=-1;

		try{
			updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateStatementString);
			updateStatement.setString(1,fr);
			updateStatement.setString(2,te);
			updateStatement.setString(3,elvn);
			updateStatement.setString(4,elvp);
			updateStatement.setString(5,ep);
			updateStatement.setString(6,a);
			updateStatement.setInt(7, id);

			toReturn=updateStatement.executeUpdate();

		} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "AnamnezaDAO:updateAnamneza " + e.getMessage());
		} finally {
			DatabaseConnection.close(updateStatement);
			DatabaseConnection.close(dbConnection);
		}

		return toReturn;
	}
}
