package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Anamneza;
import model.Diagnostic;
import model.Pacient;

public class DiagnosticDAO {
	protected static final Logger LOGGER = Logger.getLogger(DiagnosticDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO diagnostic (iddiagnostic, nume_maladie)"
			+ " VALUES (?,?)";
	private final static String getDiagnosticStatementString = "SELECT * FROM diagnostic WHERE iddiagnostic = ?";
	private final static String getDiagnosticsStatementString ="SELECT * FROM diagnostic ORDER BY iddiagnostic ASC";


	public static int addDiagnostic(Diagnostic u) {
		Connection dbConnection = DatabaseConnection.getConnection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;

		try {
			insertStatement = (PreparedStatement) dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, u.getIdDiagnostic());
			insertStatement.setString(2, u.getNumeMaladie());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "DiagnosticDAO:addDiagnostic " + e.getMessage());
		} finally {
			DatabaseConnection.close(insertStatement);
			DatabaseConnection.close(dbConnection);
		}
		return insertedId;
	}
	
	public static Diagnostic findByIdDiagnostic(int id){
		Diagnostic toReturn= null;

		Connection dbConnection = (Connection) DatabaseConnection.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;

		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(getDiagnosticStatementString);
			findStatement.setLong(1, id);
			rs = findStatement.executeQuery();
			//rs.next();
			if (rs.next()) {
				String nm = rs.getString("nume_maladie");

				toReturn = new Diagnostic(id,nm);
	        }

			

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"DiagnosticDAO:findByIdDiagnostic " + e.getMessage());
		} finally {
			if (rs != null)
			       DatabaseConnection.close(rs);
			DatabaseConnection.close(findStatement);
			DatabaseConnection.close(dbConnection);
		}
		return toReturn;
	}
	
	public static ArrayList<Diagnostic> getDiagnostics(){
		Connection dbConnection = DatabaseConnection.getConnection();
		PreparedStatement selectStatement = null;
		ResultSet rs = null;
		ArrayList<Diagnostic> diagnostics= new ArrayList<Diagnostic>();

		try{
			selectStatement = (PreparedStatement) dbConnection.prepareStatement(getDiagnosticsStatementString);
			rs = selectStatement.executeQuery();

			while(rs.next()){
				int id= rs.getInt("iddiagnostic");
				String nume = rs.getString("nume_maladie");
				Diagnostic p= new Diagnostic(id,nume);
				diagnostics.add(p);
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "DiagnosticDAO:getDiagnostics " + e.getMessage());
		} finally {
			DatabaseConnection.close(selectStatement);
			DatabaseConnection.close(dbConnection);
		}
		return diagnostics;
	}


}
