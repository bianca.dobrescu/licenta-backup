package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Anamneza;
import model.ExaminareBiomicroscopica;

public class ExaminareBiomicroscopicaDAO {
	protected static final Logger LOGGER = Logger.getLogger(ExaminareBiomicroscopicaDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO examenbiomicroscopic (idexamenbiomicroscopic, conjunctiva_sclera, cornee_ante, pupile_iris,"
			+ "cristalin, orbita_pleoape, aparat_lacrimal, idem)"
			+ " VALUES (?,?,?,?,?,?,?,?)";
	private final static String getExaminareBiomicroscopicaStatementString = "SELECT * FROM examenbiomicroscopic WHERE idem = ?";
	private final static String updateStatementString ="UPDATE examenbiomicroscopic SET conjunctiva_sclera=?,cornee_ante=?,pupile_iris=?,"
			+ "cristalin=?,orbita_pleoape=?, aparat_lacrimal=? WHERE idem=?";
	

	public static int addExaminareBiomicroscopica(ExaminareBiomicroscopica u) {
		Connection dbConnection = DatabaseConnection.getConnection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;

		try {
			insertStatement = (PreparedStatement) dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, u.getIdExaminareBiomicroscopica());
			insertStatement.setString(2, u.getConjunctiva());
			insertStatement.setString(3, u.getCornee());
			insertStatement.setString(4, u.getPupile());
			insertStatement.setString(5, u.getCristalin());
			insertStatement.setString(6, u.getOrbita());
			insertStatement.setString(7, u.getAparat());
			insertStatement.setInt(8, u.getIdExaminareMedic());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ExaminareBiomicroscopicaDAO:addExaminareBiomicroscopica " + e.getMessage());
		} finally {
			DatabaseConnection.close(insertStatement);
			DatabaseConnection.close(dbConnection);
		}
		return insertedId;
	}
	
	public static ExaminareBiomicroscopica findByIdExaminareMedic(int id){
		ExaminareBiomicroscopica toReturn= null;

		Connection dbConnection = (Connection) DatabaseConnection.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;

		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(getExaminareBiomicroscopicaStatementString);
			findStatement.setLong(1, id);
			rs = findStatement.executeQuery();
			//rs.next();
			if (rs.next()) {
				String conjunctiva = rs.getString("conjunctiva_sclera");
				String cornee = rs.getString("cornee_ante");
				String pupile = rs.getString("pupile_iris");
				String cristalin = rs.getString("cristalin");
				String orbita = rs.getString("orbita_pleoape");
				String aparat = rs.getString("aparat_lacrimal");
				int idem = rs.getInt("idem");

				toReturn = new ExaminareBiomicroscopica(id, idem, conjunctiva, cornee, pupile, cristalin, orbita, aparat);
	        }

			

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ExaminareBiomicroscopicaDAO:findByIdExaminareMedic " + e.getMessage());
		} finally {
			if (rs != null)
			       DatabaseConnection.close(rs);
			DatabaseConnection.close(findStatement);
			DatabaseConnection.close(dbConnection);
		}
		return toReturn;
	}
	
	public static int updateExaminareBiomicroscopica(int idem, String conjunctiva, String cornee, String pupile, String cristalin, String orbita, String aparat){
		Connection dbConnection = DatabaseConnection.getConnection();
		PreparedStatement updateStatement = null;
		int toReturn=-1;

		try{
			updateStatement= (PreparedStatement) dbConnection.prepareStatement(updateStatementString);
			updateStatement.setString(1,conjunctiva);
			updateStatement.setString(2,cornee);
			updateStatement.setString(3,pupile);
			updateStatement.setString(4,cristalin);
			updateStatement.setString(5,orbita);
			updateStatement.setString(6,aparat);
			updateStatement.setInt(7, idem);

			toReturn=updateStatement.executeUpdate();

		} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "ExaminareBiomicroscopicaDAO:updateExaminareBiomicroscopica " + e.getMessage());
		} finally {
			DatabaseConnection.close(updateStatement);
			DatabaseConnection.close(dbConnection);
		}

		return toReturn;
	}
}
