package start;

import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.swing.SwingUtilities;

import bll.UserBLL;
import model.User;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException
    {
    	    SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                	User u = null;
                	u = UserBLL.findUserByUsername("contadmin");
                	if( u == null) {
                		String parola = "Parola.admin123";
                		final byte[] authBytes = parola.getBytes(StandardCharsets.UTF_8);
        		        final String encoded = Base64.getEncoder().encodeToString(authBytes);
                		u = new User(99999,"contadmin", encoded, "Admin", "Yes");
                		UserBLL.addUser(u);
                	}
                    new presentation.LoginGUI().setVisible(true);
                }
            });
    	
    }
}
