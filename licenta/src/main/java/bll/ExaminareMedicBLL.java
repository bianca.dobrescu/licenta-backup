package bll;

import dao.ExaminareMedicDAO;
import model.ExaminareMedic;

public class ExaminareMedicBLL {
	public static ExaminareMedic findByIdPacient(int id) {
		ExaminareMedic o = ExaminareMedicDAO.findByIdPacient(id);
		return o;
	}

	public static int addExaminareMedic(ExaminareMedic b) {
		return ExaminareMedicDAO.addExaminareMedic(b);		
	}
	
	public static int updateExaminareMedic(int idp, String s, int nr) {
		return ExaminareMedicDAO.updateExaminareMedic(idp, s, nr);		
	}
	
	public static int updateDiagnosticExaminareMedic(int idp, int s, int nr) {
		return ExaminareMedicDAO.updateDiagnosticExaminareMedic(idp, s, nr);		
	}
}
