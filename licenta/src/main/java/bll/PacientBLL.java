package bll;

import java.util.ArrayList;

import dao.PacientDAO;
import model.Pacient;

public class PacientBLL {

	public static int addPacient(Pacient u) {
		return PacientDAO.addPacient(u);
	}
	
	public static ArrayList<Pacient> getAllPatients(){
		return PacientDAO.getPatients();
	}

}
