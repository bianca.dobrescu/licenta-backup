package bll;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import dao.ExaminareMedicDAO;
import dao.UserDAO;
import model.User;

public class UserBLL {
	public static User findUserByUsername(String username) {
		User o = UserDAO.findByUsername(username);
		return o;
	}

	public static int addUser(User u) {
		User user = UserDAO.findByUsername(u.getUsername());
        if (user == null) {
        	return UserDAO.addUser(u);
		}else {
			throw new NoSuchElementException("The user with username =" + u.getUsername() + " already exists!");
		}
	}
	
	public static int addUserPacient(String username,String parola) {
        	return UserDAO.addUserPacient(username,parola);
	}
	
	public static ArrayList<User> getAllUsersPending(){
		return UserDAO.getUsersPending();
	}
	
	public static int updateUser(int idu, String s) {
		return UserDAO.updateUser(idu, s);		
	}

}
