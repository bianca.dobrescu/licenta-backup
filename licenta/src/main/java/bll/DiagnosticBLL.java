package bll;

import java.util.ArrayList;

import dao.DiagnosticDAO;
import model.Diagnostic;
import model.Pacient;

public class DiagnosticBLL {
	public static Diagnostic findByIdDiagnostic(int id) {
		Diagnostic o = DiagnosticDAO.findByIdDiagnostic(id);
		return o;
	}

	public static int addDiagnostic(Diagnostic b) {
		return DiagnosticDAO.addDiagnostic(b);
			
	}
	
	public static ArrayList<Diagnostic> getAllDiagnostics(){
		return DiagnosticDAO.getDiagnostics();
	}
}
