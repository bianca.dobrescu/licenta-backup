package bll;

import dao.ExaminareAsistentDAO;
import model.ExaminareAsistent;
import model.ExaminareMedic;

public class ExaminareAsistentBLL {
	public static ExaminareAsistent findByIdPacient(int id) {
		ExaminareAsistent o = ExaminareAsistentDAO.findByIdPacient(id);
		return o;
	}

	public static int addExaminareAsistent(ExaminareAsistent b) {
		return ExaminareAsistentDAO.addExaminareAsistent(b);		
	}
	
	public static int updateExaminareAsistent(int idp, String s, int nr) {
		return ExaminareAsistentDAO.updateExaminareAsistent(idp, s, nr);		
	}
}
