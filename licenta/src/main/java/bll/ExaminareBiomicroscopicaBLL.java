package bll;

import dao.ExaminareBiomicroscopicaDAO;
import model.ExaminareBiomicroscopica;

public class ExaminareBiomicroscopicaBLL {
	public static ExaminareBiomicroscopica findByIdExaminareMedic(int id) {
		ExaminareBiomicroscopica o = ExaminareBiomicroscopicaDAO.findByIdExaminareMedic(id);
		return o;
	}

	public static int addExaminareBiomicroscopica(ExaminareBiomicroscopica b) {
		return ExaminareBiomicroscopicaDAO.addExaminareBiomicroscopica(b);
			
	}
	
	public static int updateExaminareBiomicroscopica(int a, String b, String c, String d, String e, String f, String g) {
		return ExaminareBiomicroscopicaDAO.updateExaminareBiomicroscopica(a,b,c,d,e,f,g);
			
	}
}
