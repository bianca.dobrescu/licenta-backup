package bll;

import dao.AnamnezaDAO;
import model.Anamneza;

public class AnamnezaBLL {
	
	public static Anamneza findByIdPacient(int idpacient) {
		Anamneza o = AnamnezaDAO.findByIdPacient(idpacient);
		return o;
	}

	public static int addAnamneza(Anamneza b) {
		return AnamnezaDAO.addAnamneza(b);
			
	}
	public static int updateAnamneza(int id, String a, String b, String c, String d, String e, String f){
		return AnamnezaDAO.updateAnamneza(id, a, b, c, d, e, f);
	}
}
