package presentation;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import bll.UserBLL;
import model.User;

public class UsersPendingTableModel extends AbstractTableModel {

	//List<User> data = new ArrayList<User>();
	List<User> data = UserBLL.getAllUsersPending();
    String colNames[] = { "Id", "Username","Rol" };
    Class<?> colClasses[] = { Integer.class, String.class, String.class };

    public UsersPendingTableModel() {
    }

    public int getRowCount() {
        return data.size();
    }

    public int getColumnCount() {
        return colNames.length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return data.get(rowIndex).getIdUser();
        }
        if (columnIndex == 1) {
            return data.get(rowIndex).getUsername();
        }
        if (columnIndex == 2) {
            return data.get(rowIndex).getRol();
        }
        return null;
    }

    public String getColumnName(int columnIndex) {
        return colNames[columnIndex];
    }

    public Class<?> getColumnClass(int columnIndex) {
        return colClasses[columnIndex];
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    public void deleteRow(int row) {
        data.remove(row);
        fireTableRowsDeleted(row, row);
    }

}
