package presentation;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import bll.UserBLL;
import model.User;

public class RegisterGUI extends JFrame {
    
    private JLabel labelUsername = new JLabel("Utilizator: ");
    private JLabel labelPassword = new JLabel("Parola: ");
    private JLabel labelRole = new JLabel("Rol: ");
    private JTextField textUsername = new JTextField(20);
    private JPasswordField fieldPassword = new JPasswordField(20);
    private JButton buttonRegister = new JButton("Cerere inregistrare");
    private JButton buttonLogout = new JButton("Inapoi la fereastra de delogare");
    String[] roleStrings = { "Asistent", "Medic" };
    JComboBox roleList = new JComboBox(roleStrings);
	

	
	
    public RegisterGUI() {
        super("Medical Application");
         
        // create a new panel with GridBagLayout manager
        final JPanel newPanel = new JPanel(new GridBagLayout());
         
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
         
        // add components to the panel
        constraints.gridx = 0;
        constraints.gridy = 1;     
        newPanel.add(labelUsername, constraints);
 
        constraints.gridx = 1;
        newPanel.add(textUsername, constraints);
         
        constraints.gridx = 0;
        constraints.gridy = 2;     
        newPanel.add(labelPassword, constraints);
         
        constraints.gridx = 1;
        newPanel.add(fieldPassword, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 3;     
        newPanel.add(labelRole, constraints);
 
        constraints.gridx = 1;
        newPanel.add(roleList, constraints);
         
        constraints.gridx = 0;
        constraints.gridy = 4;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonRegister, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 5;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonLogout, constraints);
        
        buttonLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					dispose();
					SwingUtilities.invokeLater(new Runnable() {
			            public void run() {
			                new presentation.LoginGUI().setVisible(true);
			            }
			        });
			}
		});
        
        
        
        buttonRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Random rand = new Random();
				int id = rand.nextInt(1000);
				String username = textUsername.getText();
				String parola = fieldPassword.getText();
				String rol = (String) roleList.getItemAt(roleList.getSelectedIndex());
				
				final byte[] authBytes = parola.getBytes(StandardCharsets.UTF_8);
		        final String encoded = Base64.getEncoder().encodeToString(authBytes);
				
				Object[] options = { "OK"};
				String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[.@#$%^&+=])(?=\\S+$).{8,}"; 
				if(parola.length()<8 || !parola.matches(pattern)) {
					int txt2 = JOptionPane.showOptionDialog(null, "Parola trebuie sa contina cel putin o litera mica, una mare si un caracter special! Cel putin 8 caractere!", "Atentie", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
				} else {
					if(username.equals("") || rol.equals("")) {
						int txt2 = JOptionPane.showOptionDialog(null, "Exista cel putin un field gol. Va rog sa completati toate field-urile!", "Atentie", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
					} else {
						User user = new User(id, username, encoded, rol,"Pending");
						UserBLL.addUser(user);
						Object[] options2 = { "OK"};
						int txt = JOptionPane.showOptionDialog(null, "Cererea inregistrarii unui nou cont a fost trimisa catre administrator. Va rugam asteptati activarea contului!", "Atentie", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options2, options2[0]);
							if (txt == JOptionPane.OK_OPTION) {
								dispose();
								SwingUtilities.invokeLater(new Runnable() {
						            public void run() {
						                new presentation.LoginGUI().setVisible(true);
						            }
						        });
							}
					}
					
				}
				
				
				
			    
			}
		});
         
        // set border for the panel
        newPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Inregistrare"));
         
        // add the panel to this frame
        add(newPanel);
         
        pack();
        setLocationRelativeTo(null);
    }
     
}