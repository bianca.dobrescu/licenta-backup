package presentation;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

import bll.PacientBLL;
import bll.UserBLL;
import model.Pacient;

public class AdminGUI extends JFrame {
	JLabel labelTabel = new JLabel("Cereri cont nou ");
    private JButton buttonAproba = new JButton("Aproba cererea");
    private JButton buttonRespinge = new JButton("Respinge cererea");
    private JButton buttonLogout = new JButton("Delogare");
    JTable table = new JTable(new UsersPendingTableModel());
    
    ArrayList<Pacient> pacients= PacientBLL.getAllPatients();
    
    
	

	
	
    public AdminGUI() {
        super("Medical Application");
        
        JPanel newPanel = new JPanel(new GridBagLayout());
         
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(15, 15, 15, 15);
         
        
        // add components to the panel
        constraints.gridx = 1;
        constraints.gridy = 1;     
        newPanel.add(labelTabel, constraints);
        
        
        constraints.gridx = 1;
        constraints.gridy = 2;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(new JScrollPane(table), constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonAproba, constraints);
         
        constraints.gridx = 1;
        constraints.gridy = 3;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonRespinge, constraints);
        
        
        constraints.gridx = 2;
        constraints.gridy = 0;
        constraints.anchor = GridBagConstraints.EAST;
        newPanel.add(buttonLogout, constraints);
        
        
        buttonLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					dispose();
					SwingUtilities.invokeLater(new Runnable() {
			            public void run() {
			                new presentation.LoginGUI().setVisible(true);
			            }
			        });
			}
		});
        
        
        buttonAproba.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int column = 0;
				int row = table.getSelectedRow();
				String value = table.getModel().getValueAt(row, column).toString();
				int val = Integer.parseInt(value);
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Confirmi aprobarea cererii de cont pentru utilizatorul cu username-ul " + 
				     table.getModel().getValueAt(row, 1).toString() + "?","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
					UserBLL.updateUser(val, "Yes");
					((UsersPendingTableModel)table.getModel()).deleteRow(row);
				}
			}
		});
        
        buttonRespinge.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int column = 0;
				int row = table.getSelectedRow();
				String value = table.getModel().getValueAt(row, column).toString();
				int val = Integer.parseInt(value);
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Confirmi respingerea cererii de cont pentru utilizatorul cu username-ul " + 
				     table.getModel().getValueAt(row, 1).toString() + "?","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
					UserBLL.updateUser(val, "No");
					((UsersPendingTableModel)table.getModel()).deleteRow(row);
				}
			}
		});
         
        // set border for the panel
        newPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Cautare"));
         
        // add the panel to this frame
        add(newPanel);
         
        pack();
        setLocationRelativeTo(null);
    }

}
