package presentation;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;

import bll.Mail;
import bll.UserBLL;
import model.LoggedUserInformation;
import model.User;

 
/**
 * This program demonstrates how to use JPanel in Swing.
 * @author www.codejava.net
 */
public class LoginGUI extends JFrame {
     
    private JLabel labelUsername = new JLabel("Utilizator: ");
    private JLabel labelPassword = new JLabel("Parola: ");
    private JTextField textUsername = new JTextField(20);
    private JPasswordField fieldPassword = new JPasswordField(20);
    private JButton buttonLogin = new JButton("Logare");
    private JButton buttonRegister = new JButton("Inregistrare");
    public static LoggedUserInformation info = new LoggedUserInformation(0, "", 0);
	

	
	
    public LoginGUI() {
        super("Medical Application");
         
        // create a new panel with GridBagLayout manager
        final JPanel newPanel = new JPanel(new GridBagLayout());
         
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
         
        // add components to the panel
        constraints.gridx = 0;
        constraints.gridy = 1;     
        newPanel.add(labelUsername, constraints);
 
        constraints.gridx = 1;
        newPanel.add(textUsername, constraints);
         
        constraints.gridx = 0;
        constraints.gridy = 2;     
        newPanel.add(labelPassword, constraints);
         
        constraints.gridx = 1;
        newPanel.add(fieldPassword, constraints);
         
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonLogin, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.EAST;
        newPanel.add(buttonRegister, constraints);
        
        
        buttonLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String username = textUsername.getText();
				String parola = fieldPassword.getText();
				User u = UserBLL.findUserByUsername(username);
				
				if (u != null && u.getContActiv().equals("Yes")) {
					info.setLoggedUserId(u.getIdUser());
					info.setLoggedUserRole(u.getRol());
					info.setSearchedPatientId(0);
					dispose();
					
					if(info.getLoggedUserRole().equals("Admin")) {
						dispose();
						SwingUtilities.invokeLater(new Runnable() {
				            public void run() {
				                new presentation.AdminGUI().setVisible(true);
				            }
				        });
					} 
					else {
						dispose();
						SwingUtilities.invokeLater(new Runnable() {
				            public void run() {
				                new presentation.MainGUI().setVisible(true);
				            }
				        });
					}
					
				
				} else if (u != null && u.getContActiv().equals("Pending")) {
					JOptionPane.showMessageDialog(null, "Contul inca nu a fost activat de catre administrator. Va rugam reveniti!", "Eroare", JOptionPane.ERROR_MESSAGE);
				} else if (u != null && u.getContActiv().equals("No")) {
					JOptionPane.showMessageDialog(null, "Cererea de cont a fost respinsa de catre administrator!", "Eroare", JOptionPane.ERROR_MESSAGE);
				}
				else {
					JOptionPane.showMessageDialog(null, "User sau parola gresita! Incearca din nou!", "Eroare", JOptionPane.ERROR_MESSAGE);
			    }
			    
			}
		});
        
        
        buttonRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				SwingUtilities.invokeLater(new Runnable() {
		            public void run() {
		                new presentation.RegisterGUI().setVisible(true);
		            }
		        });
			}
		});
         
        // set border for the panel
        newPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Panel Logare"));
         
        // add the panel to this frame
        add(newPanel);
         
        pack();
        setLocationRelativeTo(null);
    }
     
}