package presentation;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.font.TextAttribute;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import bll.AnamnezaBLL;
import bll.DiagnosticBLL;
import bll.ExaminareBiomicroscopicaBLL;
import bll.ExaminareMedicBLL;
import bll.PacientBLL;
import model.Anamneza;
import model.Diagnostic;
import model.ExaminareBiomicroscopica;
import model.ExaminareMedic;
import model.Pacient;

public class DoctorGUI extends JFrame {
	//labels
	private JLabel labelAnamneza = new JLabel("Anamneza: ");
	//private JLabel labelUsername = new JLabel(String.valueOf(SwingJPanelDemo.info.getLoggedUserId()) + " " + SwingJPanelDemo.info.getLoggedUserRole() + " " + String.valueOf(SwingJPanelDemo.info.getSearchedPatientId()));
	private JLabel labelFactoriRisc = new JLabel("Factori de risc: ");
	private JLabel labelTratamentEfectuat = new JLabel("Tratament efectuat: ");
	private JLabel labelAntecedenteOculare = new JLabel("Antecedente oculare: ");
	private JLabel labelExamenLaboratorValoriNormale = new JLabel("Examen de laborator: - cu valori normale:");
	private JLabel labelExamenLaboratorValoriPatologice = new JLabel("                                          - cu valori patologice: ");
	private JLabel labelExamenBiomicroscopic = new JLabel("Examen biomicroscopic: ");
	private JLabel labelExaminariParaclinice = new JLabel("Examinari paraclinice: ");
	private JLabel labelConjunctiva = new JLabel("Conjunctiva, sclera: ");
	private JLabel labelCornee = new JLabel("Cornee, camera anterioara: ");
	private JLabel labelPupile = new JLabel("Pupile, iris:");
	private JLabel labelCristalin = new JLabel("Cristalin: ");
	private JLabel labelOrbita = new JLabel("Orbita, pleoape: ");
	private JLabel labelAparatLacrimal = new JLabel("Aparat lacrimal:");
	private JLabel labelMotilitate = new JLabel("Motilitate oculara: ");
	private JLabel labelCT = new JLabel("CT: ");
	private JLabel labelStereoscopie = new JLabel("Stereoscopie: ");
	private JLabel labelFOchi = new JLabel("Fund de ochi: ");
	private JLabel labelDiagnostic = new JLabel("Diagnostic: ");
	private JLabel labelTratament = new JLabel("Tratament: ");
	
	//textfields
	private JTextField textDiagnostic = new JTextField(25);   
	private JTextField textFactoriRisc = new JTextField(25);
	private JTextField textTratamentEfectuat = new JTextField(25);
	private JTextField textAntecedenteOculare = new JTextField(25);
	private JTextField textExamenLaboratorValoriNormale = new JTextField(25);
	private JTextField textExamenLaboratorValoriPatologice = new JTextField(25);
	private JTextField textExaminariParaclinice = new JTextField(25);
	private JTextField textConjunctiva = new JTextField(25);
	private JTextField textCornee = new JTextField(25);
	private JTextField textPupile = new JTextField(25);
	private JTextField textCristalin = new JTextField(25);
	private JTextField textOrbita = new JTextField(25);
	private JTextField textAparatLacrimal = new JTextField(25);
	private JTextField textMotilitate = new JTextField(25);    
    private JTextField textCT = new JTextField(25);    
    private JTextField textStereoscopie = new JTextField(25);    
    private JTextField textFOchi = new JTextField(25);
    private JTextField textTratament = new JTextField(25);
    
    //text areas
    private TextArea areaTratament = new TextArea(5,35);
    
    //buttons
    private JButton buttonDiagnostic = new JButton("Modifica diagnostic");
    private JButton buttonAnamneza = new JButton("Modifica anamneza");
    private JButton buttonMotilitate = new JButton("Modifica motilitate oculara");
    private JButton buttonCT = new JButton("Modifica CT");
    private JButton buttonStereoscopie = new JButton("Modifica stereoscopie");
    private JButton buttonBiomicroscopie = new JButton("Modifica examen biomicroscopie");
    private JButton buttonFOchi = new JButton("Modifica fund ochi");
    private JButton buttonTratament = new JButton("Modifica tratament");   
    private JButton buttonExport = new JButton("Export"); 
    private JButton buttonLogout = new JButton("Delogare");
    private JButton buttonInapoi = new JButton("Inapoi");
    
    
    private int idpacient = LoginGUI.info.getSearchedPatientId();
    ArrayList<Diagnostic> diagnostics= DiagnosticBLL.getAllDiagnostics();
    private int idem;
	
	
    @SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
	public DoctorGUI() {
        super("Medical Application");
         
        // create a new panel with GridBagLayout manager
        JPanel newPanel = new JPanel(new GridBagLayout());
        
        int nr = diagnostics.size();
        String[] diagnosticStrings = new String[nr];
        int i=0;
        for (Diagnostic p : diagnostics) { 		      
            diagnosticStrings[i] = p.getNumeMaladie(); 
            i++;
        }
		final JComboBox diagnosticList = new JComboBox(diagnosticStrings);
        
        ExaminareMedic em = ExaminareMedicBLL.findByIdPacient(idpacient);
        Anamneza a = AnamnezaBLL.findByIdPacient(idpacient);
        ExaminareBiomicroscopica eb = ExaminareBiomicroscopicaBLL.findByIdExaminareMedic(em.getIdExaminareMedic());
        //Diagnostic d = DiagnosticBLL.findByIdDiagnostic(em.getIdDiagnostic()); 
        
        
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(5, 5, 5, 5);
        
        constraints.gridx = 3;
        constraints.gridy = 0;
        constraints.anchor = GridBagConstraints.EAST;
        newPanel.add(buttonInapoi, constraints);
        
        constraints.gridx = 4;
        constraints.gridy = 0;
        constraints.anchor = GridBagConstraints.EAST;
        newPanel.add(buttonLogout, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 1;   
        constraints.anchor = GridBagConstraints.WEST;
        Font font = labelDiagnostic.getFont();
        Map attributes = font.getAttributes();
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        labelDiagnostic.setFont(font.deriveFont(attributes));
        newPanel.add(labelDiagnostic, constraints);
        /*
        constraints.gridx = 1;
        //constraints.gridwidth = 3;
        if(em.getIdDiagnostic()==0) {
        	textDiagnostic.setText("-");
        } else {
        	textDiagnostic.setText(d.getNumeMaladie());
        }
        
        textDiagnostic.setEditable(false);
        textDiagnostic.setVisible(true);
        newPanel.add(textDiagnostic, constraints);
        */
        constraints.gridx = 1;
        diagnosticList.setSelectedIndex(em.getIdDiagnostic());
        diagnosticList.setVisible(true);
        diagnosticList.setEnabled(false);
        newPanel.add(diagnosticList, constraints);
        
        constraints.gridx = 3;
        newPanel.add(buttonDiagnostic, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 2;   
        constraints.anchor = GridBagConstraints.WEST;
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        labelAnamneza.setFont(font.deriveFont(attributes));
        newPanel.add(labelAnamneza, constraints);
        
        constraints.gridx = 3;
        newPanel.add(buttonAnamneza, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 2;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelFactoriRisc, constraints);
        
        constraints.gridx = 2;
        textFactoriRisc.setText(a.getFactoriRisc());
        textFactoriRisc.setEditable(false);
        newPanel.add(textFactoriRisc, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 3;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelTratamentEfectuat, constraints);
        
        constraints.gridx = 2;
        textTratamentEfectuat.setText(a.getTratamentEfect());
        textTratamentEfectuat.setEditable(false);
        newPanel.add(textTratamentEfectuat, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 4;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelAntecedenteOculare, constraints);
        
        constraints.gridx = 2;
        textAntecedenteOculare.setText(a.getAntecedenteOculare());
        textAntecedenteOculare.setEditable(false);
        newPanel.add(textAntecedenteOculare, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 5;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelExamenLaboratorValoriNormale, constraints);
        
        constraints.gridx = 2;
        textExamenLaboratorValoriNormale.setText(a.getELVN());
        textExamenLaboratorValoriNormale.setEditable(false);
        newPanel.add(textExamenLaboratorValoriNormale, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 6;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelExamenLaboratorValoriPatologice, constraints);
        
        constraints.gridx = 2;
        textExamenLaboratorValoriPatologice.setText(a.getELVP());
        textExamenLaboratorValoriPatologice.setEditable(false);
        newPanel.add(textExamenLaboratorValoriPatologice, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 7;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelExaminariParaclinice, constraints);
        
        constraints.gridx = 2;
        textExaminariParaclinice.setText(a.getEP());
        textExaminariParaclinice.setEditable(false);
        newPanel.add(textExaminariParaclinice, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 8;   
        constraints.anchor = GridBagConstraints.WEST;
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        labelExamenBiomicroscopic.setFont(font.deriveFont(attributes));
        newPanel.add(labelExamenBiomicroscopic, constraints);
        
        constraints.gridx = 3;
        newPanel.add(buttonBiomicroscopie, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 8;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelConjunctiva, constraints);
        
        constraints.gridx = 2;
        textConjunctiva.setText(eb.getConjunctiva());
        textConjunctiva.setEditable(false);
        newPanel.add(textConjunctiva, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 9;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelCornee, constraints);
        
        constraints.gridx = 2;
        textCornee.setText(eb.getCornee());
        textCornee.setEditable(false);
        newPanel.add(textCornee, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 10;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelPupile, constraints);
        
        constraints.gridx = 2;
        textPupile.setText(eb.getPupile());
        textPupile.setEditable(false);
        newPanel.add(textPupile, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 11;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelCristalin, constraints);
        
        constraints.gridx = 2;
        textCristalin.setText(eb.getCristalin());
        textCristalin.setEditable(false);
        newPanel.add(textCristalin, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 12;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelOrbita, constraints);
        
        constraints.gridx = 2;
        textOrbita.setText(eb.getOrbita());
        textOrbita.setEditable(false);
        newPanel.add(textOrbita, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 13;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelAparatLacrimal, constraints);
        
        constraints.gridx = 2;
        textAparatLacrimal.setText(eb.getAparat());
        textAparatLacrimal.setEditable(false);
        newPanel.add(textAparatLacrimal, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 14;   
        constraints.anchor = GridBagConstraints.WEST;
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        labelMotilitate.setFont(font.deriveFont(attributes));
        newPanel.add(labelMotilitate, constraints);
        
        constraints.gridx = 1;
        textMotilitate.setText(em.getMobilitateOculara());
        textMotilitate.setEditable(false);
        newPanel.add(textMotilitate, constraints);
        
        constraints.gridx = 3;
        newPanel.add(buttonMotilitate, constraints);
        
        
        constraints.gridx = 0;
        constraints.gridy = 15;   
        constraints.anchor = GridBagConstraints.WEST;
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        labelCT.setFont(font.deriveFont(attributes));
        newPanel.add(labelCT, constraints);
        
        constraints.gridx = 1;
        textCT.setText(em.getCT());
        textCT.setEditable(false);
        newPanel.add(textCT, constraints);
        
        constraints.gridx = 3;
        newPanel.add(buttonCT, constraints);
        
        
        constraints.gridx = 0;
        constraints.gridy = 16;   
        constraints.anchor = GridBagConstraints.WEST;
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        labelStereoscopie.setFont(font.deriveFont(attributes));
        newPanel.add(labelStereoscopie, constraints);
        
        constraints.gridx = 1;
        textStereoscopie.setText(em.getStereoscopie());
        textStereoscopie.setEditable(false);
        newPanel.add(textStereoscopie, constraints);
        
        constraints.gridx = 3;
        newPanel.add(buttonStereoscopie, constraints);
        
        
        constraints.gridx = 0;
        constraints.gridy = 17;   
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        labelFOchi.setFont(font.deriveFont(attributes));
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelFOchi, constraints);
        
        constraints.gridx = 1;
        textFOchi.setText(em.getFundOchi());
        textFOchi.setEditable(false);
        newPanel.add(textFOchi, constraints);
        
        constraints.gridx = 3;
        newPanel.add(buttonFOchi, constraints);
        
        
        constraints.gridx = 0;
        constraints.gridy = 18;   
        constraints.anchor = GridBagConstraints.WEST;
        labelTratament.setFont(font.deriveFont(attributes));
        newPanel.add(labelTratament, constraints);
 
        constraints.gridx = 1;
        areaTratament.setText(em.getTratament());
        areaTratament.setEditable(false);
        newPanel.add(areaTratament, constraints);
         
        constraints.gridx = 3;
        newPanel.add(buttonTratament, constraints);
        
        
        
        
        
        buttonLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					dispose();
					SwingUtilities.invokeLater(new Runnable() {
			            public void run() {
			                new presentation.LoginGUI().setVisible(true);
			            }
			        });
			}
		});
        
        buttonInapoi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					dispose();
					SwingUtilities.invokeLater(new Runnable() {
			            public void run() {
			                new presentation.MainGUI().setVisible(true);
			            }
			        });
			}
		});
        
        buttonAnamneza.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textFactoriRisc.setEditable(true);
				textTratamentEfectuat.setEditable(true);
				textExamenLaboratorValoriNormale.setEditable(true);
				textExamenLaboratorValoriPatologice.setEditable(true);
				textAntecedenteOculare.setEditable(true);
				textExaminariParaclinice.setEditable(true);
				textExaminariParaclinice.addKeyListener(new KeyAdapter()
			    {
			      public void keyPressed(KeyEvent e)
			      {
			        if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER)
			        {
			        	String fr = textFactoriRisc.getText();
			        	String te = textTratamentEfectuat.getText();
			        	String vn = textExamenLaboratorValoriNormale.getText();
			        	String vp = textExamenLaboratorValoriPatologice.getText();
			        	String a = textAntecedenteOculare.getText();
			        	String ep = textExaminariParaclinice.getText();
			        	
			        	AnamnezaBLL.updateAnamneza(idpacient, fr, te, vn, vp, ep, a);
			        	
			        	textFactoriRisc.setEditable(false);
						textTratamentEfectuat.setEditable(false);
						textExamenLaboratorValoriNormale.setEditable(false);
						textAntecedenteOculare.setEditable(false);
						textExamenLaboratorValoriPatologice.setEditable(false);
						textExaminariParaclinice.setEditable(false);
			        }
			      }
			    });
			}
		});
        
        
        buttonBiomicroscopie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textConjunctiva.setEditable(true);
				textCornee.setEditable(true);
				textPupile.setEditable(true);
				textCristalin.setEditable(true);
				textOrbita.setEditable(true);
				textAparatLacrimal.setEditable(true);
				textAparatLacrimal.addKeyListener(new KeyAdapter()
			    {
			      public void keyPressed(KeyEvent e)
			      {
			        if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER)
			        {
			        	String fr = textConjunctiva.getText();
			        	String te = textCornee.getText();
			        	String vn = textPupile.getText();
			        	String vp = textCristalin.getText();
			        	String a = textOrbita.getText();
			        	String ep = textAparatLacrimal.getText();
			        	
			        	ExaminareMedic em = ExaminareMedicBLL.findByIdPacient(idpacient);
			        	idem = em.getIdExaminareMedic();
			        	ExaminareBiomicroscopicaBLL.updateExaminareBiomicroscopica(idem, fr, te, vn, vp, a, ep);
			        	
			        	textConjunctiva.setEditable(false);
						textCornee.setEditable(false);
						textPupile.setEditable(false);
						textCristalin.setEditable(false);
						textOrbita.setEditable(false);
						textAparatLacrimal.setEditable(false);
			        }
			      }
			    });
			}
		});
        
        buttonMotilitate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textMotilitate.setEditable(true);
				textMotilitate.addKeyListener(new KeyAdapter()
			    {
			      public void keyPressed(KeyEvent e)
			      {
			        if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER)
			        {
                        String s = textMotilitate.getText();
			        	
			        	ExaminareMedicBLL.updateExaminareMedic(idpacient, s, 1);
			        	
			        	textMotilitate.setEditable(false);
			        }
			      }
			    });
			}
		});
        
        buttonCT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textCT.setEditable(true);
				textCT.addKeyListener(new KeyAdapter()
			    {
			      public void keyPressed(KeyEvent e)
			      {
			        if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER)
			        {
                        String s = textCT.getText();
			        	
			        	ExaminareMedicBLL.updateExaminareMedic(idpacient, s, 2);
			        	
			        	textCT.setEditable(false);
			        }
			      }
			    });
			}
		});
        
        buttonStereoscopie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textStereoscopie.setEditable(true);
				textStereoscopie.addKeyListener(new KeyAdapter()
			    {
			      public void keyPressed(KeyEvent e)
			      {
			        if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER)
			        {
                        String s = textStereoscopie.getText();
			        	
			        	ExaminareMedicBLL.updateExaminareMedic(idpacient, s, 3);
			        	
			        	textStereoscopie.setEditable(false);
			        }
			      }
			    });
			}
		});
        
        buttonFOchi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textFOchi.setEditable(true);
				textFOchi.addKeyListener(new KeyAdapter()
			    {
			      public void keyPressed(KeyEvent e)
			      {
			        if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER)
			        {
                        String s = textFOchi.getText();
			        	
			        	ExaminareMedicBLL.updateExaminareMedic(idpacient, s, 4);
			        	
			        	textFOchi.setEditable(false);
			        }
			      }
			    });
			}
		});
        
        buttonDiagnostic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				diagnosticList.setEnabled(true);
				diagnosticList.addKeyListener(new KeyAdapter()
			    {
			      public void keyPressed(KeyEvent e)
			      {
			        if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER)
			        {	
                        String val = (String) diagnosticList.getSelectedItem();
        				int valId=0;
        				for (Diagnostic p : diagnostics) { 		      
        		            if(p.getNumeMaladie().equals(val)) {
        		            	valId = p.getIdDiagnostic();
        		            }; 
        		        }
			        	diagnosticList.setSelectedIndex(valId);
			        	ExaminareMedicBLL.updateDiagnosticExaminareMedic(idpacient, valId, 5);
			        	
			        	diagnosticList.setEnabled(false);
			        }
			      }
			    });
			}
		});
        
        buttonTratament.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				areaTratament.setEditable(true);
				areaTratament.addKeyListener(new KeyAdapter()
			    {
			      public void keyPressed(KeyEvent e)
			      {
			        if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER)
			        {	
                        String s = areaTratament.getText();
			        	
			        	ExaminareMedicBLL.updateExaminareMedic(idpacient, s, 6);
			        	
			        	areaTratament.setEditable(false);
			        }
			      }
			    });
			}
		});
         
        // set border for the panel
        newPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Detalii pacient"));
         
        // add the panel to this frame
        add(newPanel);
         
        pack();
        setLocationRelativeTo(null);
    }
}
