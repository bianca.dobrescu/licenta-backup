package presentation;

import java.awt.Component;
import java.awt.GridBagConstraints;

import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import bll.PacientBLL;
import bll.XLSXReader;
import model.Pacient;


public class MainGUI extends JFrame {
	JLabel labelUsername = new JLabel("Cauta pacient ");
	//JLabel labelUsername = new JLabel(String.valueOf(SwingJPanelDemo.info.getLoggedUserId()) + " " + SwingJPanelDemo.info.getLoggedUserRole() + " " + String.valueOf(SwingJPanelDemo.info.getSearchedPatientId()));
    private JButton buttonCauta = new JButton("Informatii pacient selectat");
    private JButton buttonPacientNou = new JButton("Pacientul cautat nu exista? Introdu pacient nou");
    private JButton buttonLogout = new JButton("Delogare");
    
    ArrayList<Pacient> pacients= PacientBLL.getAllPatients();
    
    
	

	
	
    public MainGUI() {
        super("Medical Application");
        
        int nr = pacients.size();
        String[] pacientStrings = new String[nr];
        int i=0;
        for (Pacient p : pacients) { 		      
            pacientStrings[i] = p.getNumePrenume(); 
            i++;
        }
		final JComboBox pacientList = new JComboBox(pacientStrings);
         
        // create a new panel with GridBagLayout manager
        JPanel newPanel = new JPanel(new GridBagLayout());
         
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(15, 15, 15, 15);
         
        
        // add components to the panel
        constraints.gridx = 0;
        constraints.gridy = 1;     
        newPanel.add(labelUsername, constraints);
        
        constraints.gridx = 1;
        newPanel.add(pacientList, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonCauta, constraints);
         
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonPacientNou, constraints);
        
        
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.EAST;
        newPanel.add(buttonLogout, constraints);
        
        
        buttonLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					dispose();
					SwingUtilities.invokeLater(new Runnable() {
			            public void run() {
			                new presentation.LoginGUI().setVisible(true);
			            }
			        });
			}
		});
        
        
        buttonCauta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String val = (String) pacientList.getSelectedItem();
				int valId=0;
				for (Pacient p : pacients) { 		      
		            if(p.getNumePrenume().equals(val)) {
		            	valId = p.getIdPacient();
		            }; 
		        }
				LoginGUI.info.setSearchedPatientId(valId);
				//XLSXReader.getCoduriMaladii();
				if(LoginGUI.info.getLoggedUserRole().equals("Medic")) {
					dispose();
					SwingUtilities.invokeLater(new Runnable() {
			            public void run() {
			                new presentation.DoctorGUI().setVisible(true);
			            }
			        });
				} else {
					dispose();
					SwingUtilities.invokeLater(new Runnable() {
			            public void run() {
			                new presentation.AsistentGUI().setVisible(true);
			            }
			        });
				}
				
			}
		});
        
        buttonPacientNou.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
					dispose();
					SwingUtilities.invokeLater(new Runnable() {
			            public void run() {
			                new presentation.PatientInserationGUI().setVisible(true);
			            }
			        });
				
			}
		});
         
        // set border for the panel
        newPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Cautare"));
         
        // add the panel to this frame
        add(newPanel);
         
        pack();
        setLocationRelativeTo(null);
    }
}
