package presentation;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.font.TextAttribute;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import bll.AnamnezaBLL;
import bll.DiagnosticBLL;
import bll.ExaminareAsistentBLL;
import bll.ExaminareBiomicroscopicaBLL;
import bll.ExaminareMedicBLL;
import model.Anamneza;
import model.Diagnostic;
import model.ExaminareAsistent;
import model.ExaminareBiomicroscopica;
import model.ExaminareMedic;

public class AsistentGUI extends JFrame {
	private JLabel labelAnamneza = new JLabel("Anamneza: ");
	//private JLabel labelUsername = new JLabel(String.valueOf(SwingJPanelDemo.info.getLoggedUserId()) + " " + SwingJPanelDemo.info.getLoggedUserRole() + " " + String.valueOf(SwingJPanelDemo.info.getSearchedPatientId()));
	private JLabel labelFactoriRisc = new JLabel("Factori de risc: ");
	private JLabel labelTratamentEfectuat = new JLabel("Tratament efectuat: ");
	private JLabel labelAntecedenteOculare = new JLabel("Antecedente oculare: ");
	private JLabel labelExamenLaboratorValoriNormale = new JLabel("Examen de laborator: - cu valori normale:");
	private JLabel labelExamenLaboratorValoriPatologice = new JLabel("                                          - cu valori patologice: ");
	private JLabel labelExaminariParaclinice = new JLabel("Examinari paraclinice: ");
	private JLabel labelVedereAVOD = new JLabel("Vederea AV OD: ");
	private JLabel labelVedereAVOS = new JLabel("Vederea AV OS: ");
	private JLabel labelTensiuneIntraoculara = new JLabel("Tensiunea intraoculara: ");
	private JLabel labelRefractiaFaraTropicamidaOD = new JLabel("Refractia fara tropicamida OD: ");
	private JLabel labelRefractiaFaraTropicamidaOS = new JLabel("Refractia fara tropicamida OS: ");
	private JLabel labelRefractiaCuTropicamidaOD = new JLabel("Refractia cu tropicamida OD: ");
	private JLabel labelRefractiaCuTropicamidaOS = new JLabel("Refractia cu tropicamida OS: ");

	
	//textfields  
	private JTextField textFactoriRisc = new JTextField(25);
	private JTextField textTratamentEfectuat = new JTextField(25);
	private JTextField textAntecedenteOculare = new JTextField(25);
	private JTextField textExamenLaboratorValoriNormale = new JTextField(25);
	private JTextField textExamenLaboratorValoriPatologice = new JTextField(25);
	private JTextField textExaminariParaclinice = new JTextField(25);
	private JTextField textVedereAVOD = new JTextField(25);
	private JTextField textVedereAVOS = new JTextField(25);
	private JTextField textTensiuneIntraoculara = new JTextField(25);
	private JTextField textRefractiaFaraTropicamidaOD = new JTextField(25);
	private JTextField textRefractiaFaraTropicamidaOS = new JTextField(25);
	private JTextField textRefractiaCuTropicamidaOD = new JTextField(25);
	private JTextField textRefractiaCuTropicamidaOS = new JTextField(25);
	
    
    //buttons
    private JButton buttonAnamneza = new JButton("Modifica anamneza");  
    private JButton buttonVedereAVOD = new JButton("Modifica vedere AV OD");
    private JButton buttonVedereAVOS = new JButton("Modifica vedere AV OS");
    private JButton buttonTensiuneIntraoculara = new JButton("Modifica tensiune intraoculara");
    private JButton buttonRefractiaFaraTropicamidaOD = new JButton("Modifica refractia fara tropicamida OD");
    private JButton buttonRefractiaFaraTropicamidaOS = new JButton("Modifica refractia fara tropicamida OS");
    private JButton buttonRefractiaCuTropicamidaOD = new JButton("Modifica refractia cu tropicamida OD");
    private JButton buttonRefractiaCuTropicamidaOS = new JButton("Modifica refractia cu tropicamida OS");
    private JButton buttonExport = new JButton("Export"); 
    private JButton buttonLogout = new JButton("Delogare");
    private JButton buttonInapoi = new JButton("Inapoi");
    
    
    private int idpacient = LoginGUI.info.getSearchedPatientId();
	
	
    @SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
	public AsistentGUI() {
        super("Medical Application");
         
        // create a new panel with GridBagLayout manager
        JPanel newPanel = new JPanel(new GridBagLayout());
        
        
        ExaminareAsistent e = ExaminareAsistentBLL.findByIdPacient(idpacient);
        Anamneza a = AnamnezaBLL.findByIdPacient(idpacient);
        
        
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(5, 5, 5, 5);
        
        constraints.gridx = 3;
        constraints.gridy = 0;
        constraints.anchor = GridBagConstraints.EAST;
        newPanel.add(buttonInapoi, constraints);
        
        constraints.gridx = 4;
        constraints.gridy = 0;
        constraints.anchor = GridBagConstraints.EAST;
        newPanel.add(buttonLogout, constraints);
        
        Font font = labelAnamneza.getFont();
        Map attributes = font.getAttributes();
        
        constraints.gridx = 0;
        constraints.gridy = 1;   
        constraints.anchor = GridBagConstraints.WEST;
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        labelAnamneza.setFont(font.deriveFont(attributes));
        newPanel.add(labelAnamneza, constraints);
        
        constraints.gridx = 3;
        newPanel.add(buttonAnamneza, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 1;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelFactoriRisc, constraints);
        
        constraints.gridx = 2;
        textFactoriRisc.setText(a.getFactoriRisc());
        textFactoriRisc.setEditable(false);
        newPanel.add(textFactoriRisc, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 2;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelTratamentEfectuat, constraints);
        
        constraints.gridx = 2;
        textTratamentEfectuat.setText(a.getTratamentEfect());
        textTratamentEfectuat.setEditable(false);
        newPanel.add(textTratamentEfectuat, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 3;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelAntecedenteOculare, constraints);
        
        constraints.gridx = 2;
        textAntecedenteOculare.setText(a.getAntecedenteOculare());
        textAntecedenteOculare.setEditable(false);
        newPanel.add(textAntecedenteOculare, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 4;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelExamenLaboratorValoriNormale, constraints);
        
        constraints.gridx = 2;
        textExamenLaboratorValoriNormale.setText(a.getELVN());
        textExamenLaboratorValoriNormale.setEditable(false);
        newPanel.add(textExamenLaboratorValoriNormale, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 5;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelExamenLaboratorValoriPatologice, constraints);
        
        constraints.gridx = 2;
        textExamenLaboratorValoriPatologice.setText(a.getELVP());
        textExamenLaboratorValoriPatologice.setEditable(false);
        newPanel.add(textExamenLaboratorValoriPatologice, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 6;   
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(labelExaminariParaclinice, constraints);
        
        constraints.gridx = 2;
        textExaminariParaclinice.setText(a.getEP());
        textExaminariParaclinice.setEditable(false);
        newPanel.add(textExaminariParaclinice, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 7;   
        constraints.anchor = GridBagConstraints.WEST;
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        labelVedereAVOD.setFont(font.deriveFont(attributes));
        newPanel.add(labelVedereAVOD, constraints);
        
        constraints.gridx = 1;
        textVedereAVOD.setText(e.getVedereAVOD());
        textVedereAVOD.setEditable(false);
        newPanel.add(textVedereAVOD, constraints);
        
        constraints.gridx = 3;
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(buttonVedereAVOD, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 8;   
        constraints.anchor = GridBagConstraints.WEST;
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        labelVedereAVOS.setFont(font.deriveFont(attributes));
        newPanel.add(labelVedereAVOS, constraints);
        
        constraints.gridx = 1;
        textVedereAVOS.setText(e.getVedereAVOS());
        textVedereAVOS.setEditable(false);
        newPanel.add(textVedereAVOS, constraints);
        
        constraints.gridx = 3;
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(buttonVedereAVOS, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 9;   
        constraints.anchor = GridBagConstraints.WEST;
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        labelTensiuneIntraoculara.setFont(font.deriveFont(attributes));
        newPanel.add(labelTensiuneIntraoculara, constraints);
        
        constraints.gridx = 1;
        textTensiuneIntraoculara.setText(e.getTensiuneIntraoculara());
        textTensiuneIntraoculara.setEditable(false);
        newPanel.add(textTensiuneIntraoculara, constraints);
        
        constraints.gridx = 3;
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(buttonTensiuneIntraoculara, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 10;   
        constraints.anchor = GridBagConstraints.WEST;
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        labelRefractiaFaraTropicamidaOD.setFont(font.deriveFont(attributes));
        newPanel.add(labelRefractiaFaraTropicamidaOD, constraints);
        
        constraints.gridx = 1;
        textRefractiaFaraTropicamidaOD.setText(e.getRFTOD());
        textRefractiaFaraTropicamidaOD.setEditable(false);
        newPanel.add(textRefractiaFaraTropicamidaOD, constraints);
        
        constraints.gridx = 3;
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(buttonRefractiaFaraTropicamidaOD, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 11;   
        constraints.anchor = GridBagConstraints.WEST;
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        labelRefractiaFaraTropicamidaOS.setFont(font.deriveFont(attributes));
        newPanel.add(labelRefractiaFaraTropicamidaOS, constraints);
        
        constraints.gridx = 1;
        textRefractiaFaraTropicamidaOS.setText(e.getRFTOS());
        textRefractiaFaraTropicamidaOS.setEditable(false);
        newPanel.add(textRefractiaFaraTropicamidaOS, constraints);
        
        constraints.gridx = 3;
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(buttonRefractiaFaraTropicamidaOS, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 12;   
        constraints.anchor = GridBagConstraints.WEST;
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        labelRefractiaCuTropicamidaOD.setFont(font.deriveFont(attributes));
        newPanel.add(labelRefractiaCuTropicamidaOD, constraints);
        
        constraints.gridx = 1;
        textRefractiaCuTropicamidaOD.setText(e.getRCTOD());
        textRefractiaCuTropicamidaOD.setEditable(false);
        newPanel.add(textRefractiaCuTropicamidaOD, constraints);
        
        constraints.gridx = 3;
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(buttonRefractiaCuTropicamidaOD, constraints);
        
        
        constraints.gridx = 0;
        constraints.gridy = 13;   
        constraints.anchor = GridBagConstraints.WEST;
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        labelRefractiaCuTropicamidaOS.setFont(font.deriveFont(attributes));
        newPanel.add(labelRefractiaCuTropicamidaOS, constraints);
        
        constraints.gridx = 1;
        textRefractiaCuTropicamidaOS.setText(e.getRCTOS());
        textRefractiaCuTropicamidaOS.setEditable(false);
        newPanel.add(textRefractiaCuTropicamidaOS, constraints);
        
        constraints.gridx = 3;
        constraints.anchor = GridBagConstraints.WEST;
        newPanel.add(buttonRefractiaCuTropicamidaOS, constraints);
        
        
        
        buttonLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					dispose();
					SwingUtilities.invokeLater(new Runnable() {
			            public void run() {
			                new presentation.LoginGUI().setVisible(true);
			            }
			        });
			}
		});
        
        buttonInapoi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					dispose();
					SwingUtilities.invokeLater(new Runnable() {
			            public void run() {
			                new presentation.MainGUI().setVisible(true);
			            }
			        });
			}
		});
        
        buttonAnamneza.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textFactoriRisc.setEditable(true);
				textTratamentEfectuat.setEditable(true);
				textExamenLaboratorValoriNormale.setEditable(true);
				textExamenLaboratorValoriPatologice.setEditable(true);
				textAntecedenteOculare.setEditable(true);
				textExaminariParaclinice.setEditable(true);
				textExaminariParaclinice.addKeyListener(new KeyAdapter()
			    {
			      public void keyPressed(KeyEvent e)
			      {
			        if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER)
			        {
			        	String fr = textFactoriRisc.getText();
			        	String te = textTratamentEfectuat.getText();
			        	String vn = textExamenLaboratorValoriNormale.getText();
			        	String vp = textExamenLaboratorValoriPatologice.getText();
			        	String a = textAntecedenteOculare.getText();
			        	String ep = textExaminariParaclinice.getText();
			        	
			        	AnamnezaBLL.updateAnamneza(idpacient, fr, te, vn, vp, ep, a);
			        	
			        	textFactoriRisc.setEditable(false);
						textTratamentEfectuat.setEditable(false);
						textExamenLaboratorValoriNormale.setEditable(false);
						textAntecedenteOculare.setEditable(false);
						textExamenLaboratorValoriPatologice.setEditable(false);
						textExaminariParaclinice.setEditable(false);
			        }
			      }
			    });
			}
		});
        
        buttonVedereAVOD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textVedereAVOD.setEditable(true);
				textVedereAVOD.addKeyListener(new KeyAdapter()
			    {
			      public void keyPressed(KeyEvent e)
			      {
			        if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER)
			        {
			        	String v = textVedereAVOD.getText();
			        	
			        	ExaminareAsistentBLL.updateExaminareAsistent(idpacient,v,1);
			        	
						textVedereAVOD.setEditable(false);
			        }
			      }
			    });
			}
		});
        
        buttonVedereAVOS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textVedereAVOS.setEditable(true);
				textVedereAVOS.addKeyListener(new KeyAdapter()
			    {
			      public void keyPressed(KeyEvent e)
			      {
			        if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER)
			        {
			        	String v = textVedereAVOS.getText();
			        	
			        	ExaminareAsistentBLL.updateExaminareAsistent(idpacient,v,2);
			        	
						textVedereAVOS.setEditable(false);
			        }
			      }
			    });
			}
		});
        
        buttonTensiuneIntraoculara.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textTensiuneIntraoculara.setEditable(true);
				textTensiuneIntraoculara.addKeyListener(new KeyAdapter()
			    {
			      public void keyPressed(KeyEvent e)
			      {
			        if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER)
			        {
			        	String v = textTensiuneIntraoculara.getText();
			        	
			        	ExaminareAsistentBLL.updateExaminareAsistent(idpacient,v,3);
			        	
						textTensiuneIntraoculara.setEditable(false);
			        }
			      }
			    });
			}
		});
        
        buttonRefractiaFaraTropicamidaOD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textRefractiaFaraTropicamidaOD.setEditable(true);
				textRefractiaFaraTropicamidaOD.addKeyListener(new KeyAdapter()
			    {
			      public void keyPressed(KeyEvent e)
			      {
			        if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER)
			        {
			        	String v = textRefractiaFaraTropicamidaOD.getText();
			        	
			        	ExaminareAsistentBLL.updateExaminareAsistent(idpacient,v,4);
			        	
						textRefractiaFaraTropicamidaOD.setEditable(false);
			        }
			      }
			    });
			}
		});
        
        buttonRefractiaFaraTropicamidaOS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textRefractiaFaraTropicamidaOS.setEditable(true);
				textRefractiaFaraTropicamidaOS.addKeyListener(new KeyAdapter()
			    {
			      public void keyPressed(KeyEvent e)
			      {
			        if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER)
			        {
			        	String v = textRefractiaFaraTropicamidaOS.getText();
			        	
			        	ExaminareAsistentBLL.updateExaminareAsistent(idpacient,v,5);
			        	
						textRefractiaFaraTropicamidaOS.setEditable(false);
			        }
			      }
			    });
			}
		});
        
        buttonRefractiaCuTropicamidaOD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textRefractiaCuTropicamidaOD.setEditable(true);
				textRefractiaCuTropicamidaOD.addKeyListener(new KeyAdapter()
			    {
			      public void keyPressed(KeyEvent e)
			      {
			        if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER)
			        {
			        	String v = textRefractiaCuTropicamidaOD.getText();
			        	
			        	ExaminareAsistentBLL.updateExaminareAsistent(idpacient,v,6);
			        	
						textRefractiaCuTropicamidaOD.setEditable(false);
			        }
			      }
			    });
			}
		});
        
        buttonRefractiaCuTropicamidaOS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textRefractiaCuTropicamidaOS.setEditable(true);
				textRefractiaCuTropicamidaOS.addKeyListener(new KeyAdapter()
			    {
			      public void keyPressed(KeyEvent e)
			      {
			        if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER)
			        {
			        	String v = textRefractiaCuTropicamidaOS.getText();
			        	
			        	ExaminareAsistentBLL.updateExaminareAsistent(idpacient,v,7);
			        	
						textRefractiaCuTropicamidaOS.setEditable(false);
			        }
			      }
			    });
			}
		});
        
        
        
         
        // set border for the panel
        newPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Detalii pacient"));
         
        // add the panel to this frame
        add(newPanel);
         
        pack();
        setLocationRelativeTo(null);
    }
}
