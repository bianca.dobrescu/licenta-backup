package presentation;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.apache.commons.lang3.RandomStringUtils;

import bll.AnamnezaBLL;
import bll.ExaminareAsistentBLL;
import bll.ExaminareBiomicroscopicaBLL;
import bll.ExaminareMedicBLL;
import bll.Mail;
import bll.PacientBLL;
import bll.UserBLL;
import model.Anamneza;
import model.ExaminareAsistent;
import model.ExaminareBiomicroscopica;
import model.ExaminareMedic;
import model.Pacient;

public class PatientInserationGUI extends JFrame {
    //labels
    private JLabel labelNumePrenume = new JLabel("Nume, Prenume: ");
    private JLabel labelDataNasterii = new JLabel("Data nasterii: ");
    private JLabel labelCNP = new JLabel("CNP: ");
    private JLabel labelVarsta = new JLabel("Varsta: ");
    private JLabel labelAdresa = new JLabel("Adresa: ");
    private JLabel labelTelefon = new JLabel("Telefon: ");
    private JLabel labelEmail = new JLabel("Email: ");
    
    //textfields
    private JTextField textNumePrenume = new JTextField(20);
    private JTextField textDataNasterii = new JTextField(20);
    private JTextField textCNP = new JTextField(20);
    private JTextField textVarsta = new JTextField(20);
    private JTextField textAdresa = new JTextField(20);
    private JTextField textTelefon = new JTextField(20);
    private JTextField textEmail = new JTextField(20);
    
    //buttons
    private JButton buttonInregistreaza = new JButton("Inregistreaza pacient");

	
	
    public PatientInserationGUI() {
        super("Medical Application");
         
        // create a new panel with GridBagLayout manager
        final JPanel newPanel = new JPanel(new GridBagLayout());
         
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
         
        // add components to the panel
        constraints.gridx = 0;
        constraints.gridy = 1;     
        newPanel.add(labelNumePrenume, constraints);
 
        constraints.gridx = 1;
        newPanel.add(textNumePrenume, constraints);
         
        constraints.gridx = 0;
        constraints.gridy = 2;     
        newPanel.add(labelDataNasterii, constraints);
         
        constraints.gridx = 1;
        newPanel.add(textDataNasterii, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 3;     
        newPanel.add(labelCNP, constraints);
 
        constraints.gridx = 1;
        newPanel.add(textCNP, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 4;     
        newPanel.add(labelVarsta, constraints);
 
        constraints.gridx = 1;
        newPanel.add(textVarsta, constraints);
         
        constraints.gridx = 0;
        constraints.gridy = 5;     
        newPanel.add(labelAdresa, constraints);
         
        constraints.gridx = 1;
        newPanel.add(textAdresa, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 6;     
        newPanel.add(labelTelefon, constraints);
 
        constraints.gridx = 1;
        newPanel.add(textTelefon, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 7;     
        newPanel.add(labelEmail, constraints);
 
        constraints.gridx = 1;
        newPanel.add(textEmail, constraints);
         
        constraints.gridx = 0;
        constraints.gridy = 8;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonInregistreaza, constraints);
        
        
        buttonInregistreaza.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String subject = "Credentiale OftaReview";
	        	
	        	RandomStringUtils rand_gen = new RandomStringUtils();
	        	String username = rand_gen.randomAlphanumeric(8);
	        	String parola = rand_gen.randomAlphanumeric(10);
	        	final byte[] authBytes = parola.getBytes(StandardCharsets.UTF_8);
		        final String encoded = Base64.getEncoder().encodeToString(authBytes);
	        	UserBLL.addUserPacient(username,encoded);
	        	
	        	String s1 = "Username: " + username +"\n";
	        	String s2 = "Parola: " + parola +"\n" ;
	        	String s3 = "Link: http://localhost:3000/login";
	        	String body = s1 + s2 + s3;
	        	
	        	
	        	
				
				Random rand = new Random();
	        	int n = rand.nextInt(10000);
	        	
	        	String np = textNumePrenume.getText();
	        	String dn = textDataNasterii.getText();
	        	int cnp = Integer.parseInt(textCNP.getText());
	        	int v = Integer.parseInt(textVarsta.getText());
	        	String a = textAdresa.getText();
	        	int t = Integer.parseInt(textTelefon.getText());
	        	String email = textEmail.getText();
	        	
	        	Mail m = new Mail();
				boolean b = m.sendMail(email,subject,body);
	        	
	        	Pacient p = new Pacient(n,np,dn,cnp,v,a,t,email,username);
	        	PacientBLL.addPacient(p);
	        	
	        	Random rand2 = new Random();
	        	int nn = rand2.nextInt(10000);
	        	ExaminareAsistent e = new ExaminareAsistent(nn, n, "-", "-", "-", "-", "-", "-", "-");
	        	ExaminareAsistentBLL.addExaminareAsistent(e);
	        	
	        	Anamneza an = new Anamneza(nn, n, "-","-","-","-","-","-");
	        	AnamnezaBLL.addAnamneza(an);
	        	
	        	ExaminareMedic em = new ExaminareMedic(nn, n, 0, "-", "-", "-", "-", "-");
	        	ExaminareMedicBLL.addExaminareMedic(em);
	        	
	        	ExaminareBiomicroscopica eb = new ExaminareBiomicroscopica(nn, nn, "-","-","-","-","-","-");
	        	ExaminareBiomicroscopicaBLL.addExaminareBiomicroscopica(eb);
	        	
	        
				
						dispose();
						SwingUtilities.invokeLater(new Runnable() {
				            public void run() {
				                new presentation.MainGUI().setVisible(true);
				            }
				        });
			    
			}
		});
         
        // set border for the panel
        newPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Inserare pacient"));
         
        // add the panel to this frame
        add(newPanel);
         
        pack();
        setLocationRelativeTo(null);
    }
}
